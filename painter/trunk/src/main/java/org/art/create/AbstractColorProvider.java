package org.art.create;

import java.awt.Color;


public abstract class AbstractColorProvider implements IColorProvider {
   
   public Color getColor( int x, int y, int size ) {
      return getColor( x, y, size, size );
   }
   
   public Color getColor( int x, int y, int width, int height ) {
      return getColor( ( x + 0f ) / width, ( y + 0f ) / height );
   }

}
