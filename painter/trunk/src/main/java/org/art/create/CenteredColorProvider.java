package org.art.create;


public class CenteredColorProvider extends DistributionBasedColorProvider {
   
   private float radius;
   
   public CenteredColorProvider(boolean isRandom, float radius){
      super(isRandom);
      if(radius < 0 || radius > 1)
         throw new IllegalArgumentException("The input parameter must be between 0 and 1 (included).");
      
      this.radius = radius;
   }

   @Override
   protected float compute( float x, float y ) {
      return ( .5f - x ) * ( .5f - x ) + ( .5f - y ) * ( .5f - y ) - radius * radius;
   }

}
