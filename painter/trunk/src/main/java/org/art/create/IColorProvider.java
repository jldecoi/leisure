package org.art.create;

import java.awt.Color;

public interface IColorProvider {
   
   /**
    * 
    * @param x The percentage of x-axis. Must be between 0 and 1.
    * @param y The percentage of y-axis. Must be between 0 and 1.
    * @return
    */
   public Color getColor(float x, float y);

}
