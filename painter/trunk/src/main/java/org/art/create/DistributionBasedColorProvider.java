package org.art.create;

import java.awt.Color;
import java.util.Random;

public abstract class DistributionBasedColorProvider extends AbstractColorProvider {

   private boolean isRandom;
   private Random random;
   
   public DistributionBasedColorProvider(boolean isRandom){
      this.isRandom = isRandom;
      if(isRandom)
         random = new Random();
   }
   
   public boolean isRandom(){
      return isRandom;
   }
   
   @Override
   public Color getColor( float x, float y ) {
      return compute(x, y) <= (isRandom ? random.nextFloat() : .5) ? Color.WHITE : Color.BLACK;
   }
   
   protected abstract float compute(float x, float y);

}
