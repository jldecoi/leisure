package org.art.create;


public class LinearColorProvider extends DistributionBasedColorProvider {
   
   private float t;
   // Only for optimization.
   private float oneMinusT;
   
   public LinearColorProvider(boolean isRandom, float t){
      super(isRandom);
      if(t < 0 || t > 1)
         throw new IllegalArgumentException("The input parameter must be between 0 and 1 (included).");
      
      this.t = t;
      oneMinusT = 1 - t;
   }

   @Override
   protected float compute( float x, float y ) {
      return t * x + oneMinusT * y;
   }

}
