/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.art.create;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Dummy {

   private static void process(AbstractColorProvider provider, String name, int... resolutions) throws IOException{
      BufferedImage image = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_ARGB);
      
      for ( int resolution : resolutions ) {
         Graphics2D graphics = image.createGraphics();
         int length = 1024 / resolution;
         
         for ( int i = 0; i < resolution; i++ )
            for ( int j = 0; j < resolution; j++ ) {
               graphics.setColor(provider.getColor(i, j, resolution));
               graphics.fillRect(length * i, length * j, length, length);
            }

         ImageIO.write(image, "PNG", new File("src/main/resources/images/" + name + "." + resolution + ".png"));
      }
   }

   public static void main( String[] args ) throws IOException {
      /*int[] resolutions = new int[] { 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024 };
      process(new LinearColorProvider(false, .25f), "linear.deterministic", resolutions);
      process(new LinearColorProvider(true, .25f), "linear.random", resolutions);
      process(new CenteredColorProvider(false, .25f), "centered.deterministic", resolutions);
      process(new CenteredColorProvider(true, .25f), "centered.random", resolutions);*/
      
      doMain();
   }
   
   private static void doMain() throws IOException {
      BufferedReader reader = new BufferedReader(new FileReader("truzzi.txt"));
      String line = reader.readLine();
      while(line !=null){
         if(line.contains("videoId"))
            System.out.print(line + "\t");
         else if(line.contains("title"))
            System.out.println(line);
         
         line = reader.readLine();
      }
      reader.close();
   }

}
