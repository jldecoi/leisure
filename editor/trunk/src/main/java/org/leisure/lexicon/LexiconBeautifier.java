package org.leisure.lexicon;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class LexiconBeautifier {

	public static void main(String[] args) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Sections.class);
		Sections sections = (Sections) context.createUnmarshaller()
				.unmarshal(new File("src/main/resources/Lexicon.xml"));
		for (Section section : sections.getSection())
			Collections.sort(section.getEntry(), new Comparator<JAXBElement<? extends Entry>>() {

				@Override
				public int compare(JAXBElement<? extends Entry> o1, JAXBElement<? extends Entry> o2) {
					return getGreek(o1).compareTo(getGreek(o2));
				}

				private String getGreek(JAXBElement<? extends Entry> element) {
					return element.getValue().getGreek();
				}

			});
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.marshal(sections, new File("src/main/resources/Lexicon.1.xml"));
	}

}
