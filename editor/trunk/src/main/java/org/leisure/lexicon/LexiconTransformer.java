package org.leisure.lexicon;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

public class LexiconTransformer implements Closeable {

	private Sections sections;
	private BufferedWriter output;

	public LexiconTransformer(Sections sections, String outputFile) throws FileNotFoundException {
		this.sections = sections;
		this.output = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));
	}

	public void transform() throws IOException {
		output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		output.write("<html xmlns:tns=\"http://www.example.org/Lexicon\">\n");
		output.write("<head>\n");
		output.write("<meta charset=\"UTF-8\" />\n");
		output.write("<style type=\"text/css\">\n");
		output.write("table {\n");
		output.write("\tborder-collapse: collapse;\n");
		output.write("}\n");
		output.write("\n");
		output.write("table, th, td {\n");
		output.write("\tborder: 1px solid black;\n");
		output.write("}\n");
		output.write("</style>\n");
		output.write("<title>Lexicon</title>\n");
		output.write("</head>\n");
		output.write("<body>\n");
		output.write("<h2>Lexicon</h2>\n");
		for (Section section : sections.getSection())
			transformSection(section);
		output.write("\n");
		output.write("</body>\n");
		output.write("</html>\n");
	}

	private void transformSection(Section section) throws IOException {
		output.write("<h3>" + section.getTitle() + "</h3>\n");
		output.write("<table><thead><tr><th>Greek</th><th>Italian</th><th>Source</th></tr></thead><tbody>\n");
		for (JAXBElement<? extends Entry> element : section.getEntry())
			transformElement(element);
		output.write("</tbody></table>\n");
	}

	private void transformElement(JAXBElement<? extends Entry> element) throws IOException {
		Entry entry = element.getValue();
		if (entry instanceof Noun)
			transformNoun((Noun) entry);
		else if (entry instanceof Noun)
			transformVerb((Verb) entry);
		else
			transformEntry(entry);
	}

	private void transformNoun(Noun noun) throws IOException {
		output.write("<tr>\n");
		output.write("<td>" + getArticle(noun.getGender(), noun.isPlural()) + " " + noun.getGreek() + "</td>\n");
		output.write("<td>" + noun.getItalian() + "</td>\n");
		output.write("<td>" + transformSources(noun.getSource()) + "</td>\n");
		output.write("</tr>\n");
	}

	private String getArticle(Gender gender, boolean plural) {
		switch (gender) {
		case FEMALE:
			return plural ? "ἁι" : "̔η";
		case MALE:
			return plural ? "ὁι" : "̔ο";
		default:
			return plural ? "τὰ" : "τὸ";
		}
	}

	private void transformVerb(Verb verb) throws IOException {
		output.write("<tr>\n");
		output.write("<td>" + verb.getGreek() + "</td>\n");
		output.write("<td>" + verb.getItalian() + "</td>\n");
		output.write("<td>" + transformSources(verb.getSource()) + "</td>\n");
		output.write("</tr>\n");
	}

	private void transformEntry(Entry entry) throws IOException {
		output.write("<tr>\n");
		output.write("<td>" + entry.getGreek() + "</td>\n");
		output.write("<td>" + entry.getItalian() + "</td>\n");
		output.write("<td>" + transformSources(entry.getSource()) + "</td>\n");
		output.write("</tr>\n");
	}

	private String transformSources(List<String> sources) {
		return String.join(", ", sources);
	}

	@Override
	public void close() throws IOException {
		output.close();
	}

	public static void main(String[] args) throws JAXBException, IOException {
		LexiconTransformer transformer = new LexiconTransformer((Sections) JAXBContext.newInstance(Sections.class)
				.createUnmarshaller().unmarshal(new File("src/main/resources/Lexicon.xml")),
				"src/main/resources/Lexicon.html");
		transformer.transform();
		transformer.close();
	}

}
