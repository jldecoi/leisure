package org.editor.core;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.editor.core.data.Mapping;
import org.editor.core.data.Mappings;


public class MappingsAdapter {

   private Map<Entry<Character, Integer>, Mapping> mappings;

   public MappingsAdapter( Mappings mappings ) {
      this.mappings = getMappings(mappings);
   }

   private Map<Entry<Character, Integer>, Mapping> getMappings( Mappings mappings ) {
      Map<Entry<Character, Integer>, Mapping> toReturn = new HashMap<Entry<Character, Integer>, Mapping>();

      for ( Mapping mapping : mappings.getMapping() ) {
         char from = MappingUtils.getFrom(mapping);
         int mask = MappingUtils.getMask(mapping);
         Entry<Character, Integer> entry = new SimpleEntry<Character, Integer>(from, mask);
         if ( toReturn.containsKey(entry) )
            throw new RuntimeException("At least two mappings are available for character '" + from + "' and mask " + mask + "."); //else

         toReturn.put(entry, mapping);
      }

      return toReturn;
   }

   public Set<Entry<Character, Integer>> entrySet() {
      return mappings.keySet();
   }

   public String getTo( Entry<Character, Integer> entry ) {
      return getTo(entry.getKey(), entry.getValue());
   }

   public Integer getKeyEvent( Entry<Character, Integer> entry ) {
      return getKeyEvent(entry.getKey(), entry.getValue());
   }

   public String getLabel( Entry<Character, Integer> entry ) {
      return getLabel(entry.getKey(), entry.getValue());
   }

   public String getTo( char c, int mask ) {
      Mapping mapping = getMapping(c, mask);
      return mapping == null ? null : MappingUtils.getTo(mapping);
   }

   public Integer getKeyEvent( char c, int mask ) {
      return MappingUtils.getKeyEvent(getMapping(c, mask));
   }

   public String getLabel( char c, int mask ) {
      return MappingUtils.getLabel(getMapping(c, mask));
   }

   private Mapping getMapping( char c, int mask ) {
      return mappings.get(new SimpleEntry<Character, Integer>(c, mask));
   }

}
