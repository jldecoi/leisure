package org.editor.core;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map.Entry;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.editor.core.data.Mappings;


public class Editor extends JFrame {

   private static final long   serialVersionUID    = 7640927113121981621L;

   private static final String DEFAULT_CONFIG_FILE = "src/main/resources/Editor.xml";

   public Editor( String configFile ) throws FileNotFoundException, JAXBException {
      MappingsAdapter mappings = new MappingsAdapter(getMappings(configFile));
      JTextArea area = new JTextArea() {

         private static final long serialVersionUID = 472500108737108201L;

         public void replaceSelection( String content ) {
            super.replaceSelection(map(content));
         }

         private String map( String content ) {
            StringBuilder toReturn = new StringBuilder();
            for ( char c : content.toCharArray() ) {
               String to = mappings.getTo(c, 0);
               toReturn.append(to == null ? c : to);
            }
            return toReturn.toString();
         }

      };
      area.setFont(new Font(Font.DIALOG, Font.PLAIN, 20));

      AbstractAction action = new AbstractAction() {

         private static final long serialVersionUID = 4146597655443412139L;

         public void actionPerformed( ActionEvent e ) {
            // "e.getActionCommand()" always has length 1.
            area.replaceSelection("" + mappings.getTo(e.getActionCommand().charAt(0), toMask(e.getModifiers())));
         }

         private int toMask( int modifiers ) {
            int toReturn = 0;

            if ( (modifiers & ActionEvent.ALT_MASK) != 0 ) toReturn |= InputEvent.ALT_DOWN_MASK;
            if ( (modifiers & ActionEvent.CTRL_MASK) != 0 ) toReturn |= InputEvent.CTRL_DOWN_MASK;
            if ( (modifiers & ActionEvent.META_MASK) != 0 ) toReturn |= InputEvent.META_DOWN_MASK;
            if ( (modifiers & ActionEvent.SHIFT_MASK) != 0 ) toReturn |= InputEvent.SHIFT_DOWN_MASK;

            return toReturn;
         }

      };

      InputMap focused = area.getInputMap(JComponent.WHEN_FOCUSED);
      ActionMap actionMap = area.getActionMap();
      for ( Entry<Character, Integer> entry : mappings.entrySet() ) {
         int mask = entry.getValue();
         if ( mask != 0 ) {
            String label = mappings.getLabel(entry);
            focused.put(KeyStroke.getKeyStroke(mappings.getKeyEvent(entry), mask), label);
            actionMap.put(label, action);
         }
      }

      getContentPane().add(new JScrollPane(area));
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setSize(500, 500);
      setVisible(true);
   }

   private Mappings getMappings( String configFile ) throws FileNotFoundException, JAXBException {
      return (Mappings)JAXBContext.newInstance(Mappings.class).createUnmarshaller().unmarshal(getInputStream(configFile));
   }

   private InputStream getInputStream( String configFile ) throws FileNotFoundException {
      File file = new File(configFile);
      if ( file.exists() ) return new BufferedInputStream(new FileInputStream(file)); //else

      return Editor.class.getClassLoader().getResourceAsStream(configFile);
   }

   public static void main( String[] args ) throws FileNotFoundException, JAXBException {
      switch ( args.length ) {
      case 0:
         new Editor(DEFAULT_CONFIG_FILE);
         break;
      case 1:
         new Editor(args[0]);
         break;
      default:
         System.err.println("Usage: <PROGRAM_NAME> [CONFIG_FILE]");
         System.exit(1);
      }
   }

}