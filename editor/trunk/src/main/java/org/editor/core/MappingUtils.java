package org.editor.core;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import org.editor.core.data.Mapping;


public class MappingUtils {

   public static char getFrom( Mapping mapping ) {
      return getChar(mapping.getFrom());
   }

   public static String getTo( Mapping mapping ) {
      return mapping.getTo();
   }

   private static char getChar( String s ) {
      return s.charAt(0);
   }

   public static int getKeyEvent( Mapping mapping ) {
      char from = getFrom(mapping);
      switch ( from ) {
      case 'a':
      case 'A':
         return KeyEvent.VK_A;
      case 'c':
      case 'C':
         return KeyEvent.VK_C;
      case 'e':
      case 'E':
         return KeyEvent.VK_E;
      case 'i':
      case 'I':
         return KeyEvent.VK_I;
      case 'o':
      case 'O':
         return KeyEvent.VK_O;
      case 's':
      case 'S':
         return KeyEvent.VK_S;
      case 'u':
      case 'U':
         return KeyEvent.VK_U;
      case 'z':
      case 'Z':
         return KeyEvent.VK_Z;
      case ' ':
         return KeyEvent.VK_SPACE;
      default:
         throw new RuntimeException("No key event for character '" + from + "' specified. Extend this program.");
      }
   }

   public static int getMask( Mapping mapping ) {
      int toReturn = 0;

      if ( mapping.isShift() ) toReturn |= InputEvent.SHIFT_DOWN_MASK;
      if ( mapping.isCtrl() ) toReturn |= InputEvent.CTRL_DOWN_MASK;
      if ( mapping.isMeta() ) toReturn |= InputEvent.META_DOWN_MASK;
      if ( mapping.isAlt() ) toReturn |= InputEvent.ALT_DOWN_MASK;

      return toReturn;
   }

   public static String getLabel( Mapping mapping ) {
      StringBuilder toReturn = new StringBuilder();

      if ( mapping.isShift() ) toReturn.append("SHIFT_");
      if ( mapping.isCtrl() ) toReturn.append("CTRL_");
      if ( mapping.isMeta() ) toReturn.append("META_");
      if ( mapping.isAlt() ) toReturn.append("ALT_");

      toReturn.append(mapping.getFrom());
      return toReturn.toString();
   }

}
