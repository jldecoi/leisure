package org.books.util;

import java.util.Random;

import org.books.util.EANUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class TEANUtils {
   
   private static Random random;

   @BeforeClass
   public static void setUp(){
      random = new Random();
   }
   
   @Test
   public void test10() {
      for(int i=0; i<100; i++){
         String EAN = next(true);
         EAN += EANUtils.computeCheckDigit10(EAN);
         Assert.assertTrue(EANUtils.isCorrect(EAN));
         Assert.assertTrue(EANUtils.isCorrect(EANUtils.to13(EAN)));
      }
   }
   
   @Test
   public void test13() {
      for(int i=0; i<100; i++){
         String EAN = next(false);
         EAN += EANUtils.computeCheckDigit13(EAN);
         Assert.assertTrue(EANUtils.isCorrect(EAN));
         Assert.assertTrue(EANUtils.isCorrect(EANUtils.to10(EAN)));
      }
   }
   
   private String next(boolean ten){
      StringBuilder toReturn = new StringBuilder();
      for(int i=1; i<(ten ? 10 : 13); i++)
         toReturn.append(random.nextInt(10));
      return toReturn.toString();
   }

}