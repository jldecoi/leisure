CREATE TABLE works_books(
	work INTEGER NOT NULL,
	book BIGINT NOT NULL,
	CONSTRAINT works_books_pk PRIMARY KEY(work, book),
	FOREIGN KEY(work) REFERENCES works(id),
	FOREIGN KEY(book) REFERENCES books(id)
)