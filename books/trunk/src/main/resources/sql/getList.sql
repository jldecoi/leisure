SELECT t1.name, t1.statistics, t1.title, t1.published, t1.owned, t2.title, t2.pages, t2.price, t2.ean, t2.url, t2.published, t2.publisher
FROM (
	SELECT t1.name, t1.statistics, t2.title, t2.published, t2.owned, t2.id
	FROM works t2
	LEFT JOIN authors t1
	ON t1.id = t2.author
) t1
LEFT JOIN (
	SELECT t1.work, t2.title, t2.pages, t2.price, t2.ean, t2.url, t2.published, t2.publisher
	FROM works_books t1, books t2
	WHERE t1.book = t2.id
) t2
ON t1.id = t2.work
ORDER BY t1.name, t1.title, t2.title