package org.util;

import java.util.Iterator;

public abstract class MappingIterator<From, To> extends AbstractIterator<To> {

	private Iterator<From> inner;
	
	public MappingIterator(Iterator<From> inner){
		this.inner = inner;
	}
	
	@Override
	protected To getNext() {
		return inner.hasNext() ? map(inner.next()) : null;
	}
	
	protected abstract To map(From from);

}
