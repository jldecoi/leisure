package org.util;

import java.util.Iterator;


public abstract class FilterIterator<DataType> extends AbstractIterator<DataType> {
   
   private Iterator<DataType> inner;
   
   public FilterIterator(Iterator<DataType> inner){
      this.inner = inner;
   }

   @Override
   protected DataType getNext() {
      DataType toReturn;
      
      do{
         if(!inner.hasNext())
            return null; //else
         
         toReturn = inner.next();
      }
      while(filterOut(toReturn));
      
      return toReturn;
   }
   
   protected abstract boolean filterOut(DataType element);

}
