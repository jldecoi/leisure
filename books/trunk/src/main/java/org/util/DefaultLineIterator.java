package org.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


public class DefaultLineIterator extends LineIterator<String> {
   
   public DefaultLineIterator(BufferedReader reader){
      super(reader);
   }
   
   public DefaultLineIterator(InputStream stream){
      super(stream);
   }
   
   public DefaultLineIterator(File file) throws FileNotFoundException{
      super(file);
   }
   
   public DefaultLineIterator(String file) throws FileNotFoundException{
      super(file);
   }

   @Override
   protected String parseLine( String line ) {
      return line;
   }

}
