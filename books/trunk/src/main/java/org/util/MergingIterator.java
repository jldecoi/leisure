package org.util;

import java.util.Iterator;
import java.util.List;

public abstract class MergingIterator<ToMerge, Merged> extends AbstractIterator<Merged> {
	
	private GroupingIterator<ToMerge> inner;
	
	public MergingIterator(Iterator<ToMerge> iterator){
		inner = new GroupingIterator<ToMerge>(iterator){

			@Override
			protected boolean belongTogether(ToMerge first, ToMerge second) {
				return MergingIterator.this.belongTogether(first, second);
			}
			
		};
	}

	@Override
	protected Merged getNext() {
		return inner.hasNext() ? merge(inner.next()) : null;
	}
	
	/**
	 * <b>NOTE:</b> Implementations must be symmstric. In other words, you have to make sure that, for each <tt>first</tt> and <tt>second</tt>,
	 * <tt>belongTogether(first, second) = belongTogether(second, first)</tt>.
	 * @param first
	 * @param second
	 * @return
	 */
	protected abstract boolean belongTogether(ToMerge first, ToMerge second);
	
	protected abstract Merged merge(List<ToMerge> list);

}
