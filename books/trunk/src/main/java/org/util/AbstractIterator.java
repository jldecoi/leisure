package org.util;

import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class AbstractIterator<DataType> implements Iterator<DataType> {

   private DataType next;
   private boolean  nextConsumed;

   public AbstractIterator() {
      nextConsumed = true;
   }

   /**
    * @return The following element or <tt>null</tt> if there is not any.
    */
   protected abstract DataType getNext();

   public boolean hasNext() {
      if ( nextConsumed ) {
         next = getNext();
         nextConsumed = false;
      }

      return next != null;
   }

   public DataType next() {
      if ( !hasNext() ) throw new NoSuchElementException(); //else

      nextConsumed = true;
      return next;
   }

   public void remove() {
      throw new UnsupportedOperationException();
   }

}
