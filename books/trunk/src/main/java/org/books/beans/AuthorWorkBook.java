package org.books.beans;

public class AuthorWorkBook {
   
   private String name;
   private String statistics;
   
   private String workTitle;
   private String workPublished;
   private boolean owned;
   
   private String bookTitle;
   private String pages;
   private int price;
   private long ean;
   private String url;
   private String bookPublished;
   private String publisher;
   
   public String getName() {
      return name;
   }
   
   public void setName( String name ) {
      this.name = name;
   }
   
   public String getStatistics() {
      return statistics;
   }
   
   public void setStatistics( String statistics ) {
      this.statistics = statistics;
   }
   
   public String getWorkTitle() {
      return workTitle;
   }
   
   public void setWorkTitle( String workTitle ) {
      this.workTitle = workTitle;
   }
   
   public String getWorkPublished() {
      return workPublished;
   }
   
   public void setWorkPublished( String workPublished ) {
      this.workPublished = workPublished;
   }
   
   public boolean isOwned() {
      return owned;
   }
   
   public void setOwned( boolean owned ) {
      this.owned = owned;
   }
   
   public String getBookTitle() {
      return bookTitle;
   }
   
   public void setBookTitle( String bookTitle ) {
      this.bookTitle = bookTitle;
   }
   
   public String getPages() {
      return pages;
   }
   
   public void setPages( String pages ) {
      this.pages = pages;
   }
   
   public int getPrice() {
      return price;
   }
   
   public void setPrice( int price ) {
      this.price = price;
   }
   
   public long getEan() {
      return ean;
   }
   
   public void setEan( long ean ) {
      this.ean = ean;
   }
   
   public String getUrl() {
      return url;
   }
   
   public void setUrl( String url ) {
      this.url = url;
   }
   
   public String getBookPublished() {
      return bookPublished;
   }
   
   public void setBookPublished( String bookPublished ) {
      this.bookPublished = bookPublished;
   }
   
   public String getPublisher() {
      return publisher;
   }
   
   public void setPublisher( String publisher ) {
      this.publisher = publisher;
   }

}
