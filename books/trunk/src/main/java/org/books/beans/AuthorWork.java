package org.books.beans;

import java.util.List;

public class AuthorWork {
   
   private String name;
   private String statistics;
   
   private String workTitle;
   private String workPublished;
   private boolean owned;
   private List<Book> editions;
   
   public String getName() {
      return name;
   }
   
   public void setName( String name ) {
      this.name = name;
   }
   
   public String getStatistics() {
      return statistics;
   }
   
   public void setStatistics( String statistics ) {
      this.statistics = statistics;
   }
   
   public String getWorkTitle() {
      return workTitle;
   }
   
   public void setWorkTitle( String workTitle ) {
      this.workTitle = workTitle;
   }
   
   public String getWorkPublished() {
      return workPublished;
   }
   
   public void setWorkPublished( String workPublished ) {
      this.workPublished = workPublished;
   }
   
   public boolean isOwned() {
      return owned;
   }
   
   public void setOwned( boolean owned ) {
      this.owned = owned;
   }
   
   public List<Book> getEditions() {
      return editions;
   }
   
   public void setEditions( List<Book> editions ) {
      this.editions = editions;
   }

}
