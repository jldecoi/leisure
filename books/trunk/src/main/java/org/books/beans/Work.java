package org.books.beans;

import java.util.List;

public class Work {
   
   private String title;
   private String published;
   private boolean owned;
   private List<Book> editions;
   
   public String getTitle() {
      return title;
   }
   
   public void setTitle( String title ) {
      this.title = title;
   }
   
   public String getPublished() {
      return published;
   }
   
   public void setPublished( String published ) {
      this.published = published;
   }
   
   public boolean isOwned() {
      return owned;
   }
   
   public void setOwned( boolean owned ) {
      this.owned = owned;
   }
   
   public List<Book> getEditions() {
      return editions;
   }
   
   public void setEditions( List<Book> editions ) {
      this.editions = editions;
   }

}
