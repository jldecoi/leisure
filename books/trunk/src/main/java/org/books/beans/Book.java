package org.books.beans;


public class Book {
   
   private String title;
   private String pages;
   private int price;
   private long ean;
   private String url;
   private String published;
   private String publisher;
   
   public String getTitle() {
      return title;
   }
   
   public void setTitle( String title ) {
      this.title = title;
   }
   
   public String getPages() {
      return pages;
   }
   
   public void setPages( String pages ) {
      this.pages = pages;
   }
   
   public int getPrice() {
      return price;
   }
   
   public void setPrice( int price ) {
      this.price = price;
   }
   
   public long getEan() {
      return ean;
   }
   
   public void setEan( long ean ) {
      this.ean = ean;
   }
   
   public String getUrl() {
      return url;
   }
   
   public void setUrl( String url ) {
      this.url = url;
   }
   
   public String getPublished() {
      return published;
   }
   
   public void setPublished( String published ) {
      this.published = published;
   }
   
   public String getPublisher() {
      return publisher;
   }
   
   public void setPublisher( String publisher ) {
      this.publisher = publisher;
   }

}
