package org.books.beans;

import java.util.List;

public class Author {
   
   private String name;
   private String statistics;
   private List<Work> works;
   
   public String getName() {
      return name;
   }
   
   public void setName( String name ) {
      this.name = name;
   }
   
   public String getStatistics() {
      return statistics;
   }
   
   public void setStatistics( String statistics ) {
      this.statistics = statistics;
   }
   
   public List<Work> getWorks() {
      return works;
   }
   
   public void setWorks( List<Work> works ) {
      this.works = works;
   }

}
