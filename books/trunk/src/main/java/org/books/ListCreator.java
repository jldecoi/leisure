package org.books;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.books.beans.Author;
import org.books.beans.AuthorWork;
import org.books.beans.AuthorWorkBook;
import org.books.beans.Book;
import org.books.beans.Work;
import org.books.util.DBUtils;
import org.util.MergingIterator;
import org.util.ResultSetIterator;


public class ListCreator implements Iterable<Author> {
   
   private static final String QUERY_FILE = "src/main/resources/sql/getList.sql";
   
   @Override
   public Iterator<Author> iterator() {
      try {
         return new MergingIterator<AuthorWork, Author>(new MergingIterator<AuthorWorkBook, AuthorWork>(new ResultSetIterator<AuthorWorkBook>(DBUtils.runQuery(new File(QUERY_FILE))){

            @Override
            protected AuthorWorkBook getNext( ResultSet results ) throws SQLException {
               AuthorWorkBook toReturn = new AuthorWorkBook();
               toReturn.setName(results.getString(1));
               toReturn.setStatistics(results.getString(2));
               
               toReturn.setWorkTitle(results.getString(3));
               toReturn.setWorkPublished(results.getString(4));
               toReturn.setOwned(results.getBoolean(5));
               
               toReturn.setBookTitle(results.getString(6));
               toReturn.setPages(results.getString(7));
               toReturn.setPrice(results.getInt(8));
               toReturn.setEan(results.getLong(9));
               toReturn.setUrl(results.getString(10));
               toReturn.setBookPublished(results.getString(11));
               toReturn.setPublisher(results.getString(12));

               return toReturn;
            }
            
         }){

            @Override
            protected boolean belongTogether( AuthorWorkBook first, AuthorWorkBook second ) {
               return ListCreator.equals(first.getName(), second.getName())
                     && ListCreator.equals(first.getStatistics(), second.getStatistics())
                     && ListCreator.equals(first.getWorkTitle(), second.getWorkTitle())
                     && ListCreator.equals(first.getWorkPublished(), second.getWorkPublished())
                     && first.isOwned() == second.isOwned();
            }

            @Override
            protected AuthorWork merge( List<AuthorWorkBook> list ) {
               // "list" is never empty.
               AuthorWorkBook first = list.get(0);
               
               AuthorWork toReturn = new AuthorWork();
               toReturn.setName(first.getName());
               toReturn.setStatistics(first.getStatistics());
               toReturn.setWorkTitle(first.getWorkTitle());
               toReturn.setWorkPublished(first.getWorkPublished());
               toReturn.setOwned(first.isOwned());

               List<Book> editions = new ArrayList<Book>();
               if(nonNull(list)){
                  for(AuthorWorkBook item : list){
                     Book book = new Book();

                     book.setTitle(item.getBookTitle());
                     book.setPages(item.getPages());
                     book.setPrice(item.getPrice());
                     book.setEan(item.getEan());
                     book.setUrl(item.getUrl());
                     book.setPublished(item.getBookPublished());
                     book.setPublisher(item.getPublisher());
                     
                     editions.add(book);
                  }
               }
               toReturn.setEditions(editions);

               return toReturn;
            }
            
            private boolean nonNull(List<AuthorWorkBook> list){
               if(list.size() != 1)
                  return true; //else

               // "list" is never empty.
               return list.get(0).getBookTitle() != null;
            }
            
         }){

            @Override
            protected boolean belongTogether( AuthorWork first, AuthorWork second ) {
               return ListCreator.equals(first.getName(), second.getName())
                     && ListCreator.equals(first.getStatistics(), second.getStatistics());
            }

            @Override
            protected Author merge( List<AuthorWork> list ) {
               // "list" is never empty.
               AuthorWork first = list.get(0);
               
               Author toReturn = new Author();
               toReturn.setName(first.getName());
               toReturn.setStatistics(first.getStatistics());
               
               List<Work> works = new ArrayList<Work>();
               for(AuthorWork item : list){
                  Work book = new Work();

                  book.setTitle(item.getWorkTitle());
                  book.setPublished(item.getWorkPublished());
                  book.setOwned(item.isOwned());
                  book.setEditions(item.getEditions());
                  
                  works.add(book);
               }
               toReturn.setWorks(works);
               return toReturn;
            }
            
         };
      }
      catch ( ClassNotFoundException | SQLException | IOException e ) {
         throw new RuntimeException(e);
      }
   }
   
   private static boolean equals(String s1, String s2){
      if(s1 == null)
         return s2 == null; //else
      
      return s1.equals(s2);
   }
   
   public static void main( String[] args ){
      Iterator<Author> iterator = new ListCreator().iterator();
      
      System.out.println("<!DOCTYPE html>");
      System.out.println("<html>");
      System.out.println("<head>");
      System.out.println("<meta charset=\"UTF-8\">");
      System.out.println("<title>List</title>");
      System.out.println("</head>");
      System.out.println("<body>");
      System.out.println();

      while(iterator.hasNext())
         printAuthor(iterator.next());
      
      System.out.println("</body>");
      System.out.println("</html>");
   }
   
   private static void printAuthor(Author author){
	   String name = author.getName();
	   List<Work> works = author.getWorks();
	   if(name == null){
		   // "size" is not 0.
		   for(Work work : works)
			   printWork(work, true);
	   }
	   else {
		   System.out.println("\t<h3>" + name + "</h3>");
		   if(works.size() != 0){
			   System.out.println("\t<ul>");

			   for(Work work : works)
				   printWork(work, false);

			   System.out.println("\t</ul>");
			   System.out.println();
		   }
	   }
   }
   
   private static void printWork(Work work, boolean anonymous){
	   boolean owned = work.isOwned();
	   List<Book> editions = work.getEditions();
	   boolean notEmpty = editions.size() != 0;
	   if(anonymous){
		   System.out.print("\t<h3><i>" + work.getTitle() + "</i>");
		   if(owned)
			   System.out.print(" <img alt=\"Check.png\" src=\"Check.png\" width=\"15\" height=\"15\">");
		   System.out.println("</h3>");
		   if(notEmpty){
			   System.out.println("\t<ul>");
			   
			   for(Book edition : editions)
				   printBook(edition, true);
			   
			   System.out.println("\t</ul>");
		   }
		   System.out.println();
	   }
	   else {
		   System.out.print("\t\t<li><i>" + work.getTitle() + "</i>");
		   if(owned)
			   System.out.print(" <img alt=\"Check.png\" src=\"Check.png\" width=\"15\" height=\"15\">");
		   if(notEmpty){
			   System.out.println();
			   System.out.println("\t\t\t<ul>");
			   
			   for(Book edition : editions)
				   printBook(edition, false);
			   
			   System.out.println("\t\t\t</ul>");
			   System.out.print("\t\t");
		   }
		   System.out.println("</li>");
	   }
   }
   
   private static void printBook(Book book, boolean anonymous){
	   String title = book.getTitle();
	   String url = book.getUrl();
	   String publisher = book.getPublisher();
	   String published = book.getPublished();
	   String pages = book.getPages();
	   long ean = book.getEan();
	   
	   String titleString = url == null ? title : ("<a href=\"" + url + "\">" + title + "</a>");
	   String publishedString = published == null ? "" : (", " + published);
	   String pagesString = pages == null ? "" : (", pp. " + pages);
	   String eanString = ean == 0 ? "" : (", ISBN " + ean);
	   
	   if(!anonymous)
		   System.out.print("\t\t");
	   System.out.println("\t\t<li><i>" + titleString + "</i>, " + publisher + publishedString + pagesString + eanString + ".</li>");
   }
   
}
