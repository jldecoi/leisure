package org.books.util;


public class EANUtils {
   
   private final static int PREFIX = 978;
   
   public static boolean isCorrect(String EAN){
      return isCorrect10(EAN) || isCorrect13(EAN);
   }
   
   public static boolean isCorrect10(String EAN){
      if(EAN == null || EAN.length() != 10)
         return false; //else
      
      int sum = 0;
      for(int i=0; i<9; i++)
         sum += (10 - i) * Integer.parseInt("" + EAN.charAt(i));
      
      char c = EAN.charAt(9);
      sum += c == 'X' ? 10 : Integer.parseInt("" + c);
      return sum % 11 == 0;
   }
   
   public static boolean isCorrect13(String EAN){
      if(EAN == null || EAN.length() != 13)
         return false; //else
      
      int sum = 0;
      boolean one = true;
      for(int i=0; i<13; i++){
         sum += (one ? 1 : 3) * Integer.parseInt("" + EAN.charAt(i));
         one = !one;
      }
      return sum % 10 == 0;
   }
   
   public static String to10(String EAN){
      if(!isCorrect13(EAN))
         throw new IllegalArgumentException("Wrong input EAN."); //else

      String toReturn = EAN.substring(3, 12);
      return toReturn + computeCheckDigit10(toReturn);
   }
   
   public static String to13(String EAN){
      if(!isCorrect10(EAN))
         throw new IllegalArgumentException("Wrong input EAN."); //else
      
      String toReturn = PREFIX + EAN.substring(0, 9);
      return toReturn + computeCheckDigit13(toReturn);
   }
   
   public static char computeCheckDigit10(String EAN){
      if(EAN == null || EAN.length() != 9)
         throw new IllegalArgumentException("The input string must consist of exactly 9 digits."); //else
      
      int sum = 0;
      for(int i=0; i<9; i++)
         sum += (10 - i) * Integer.parseInt("" + EAN.charAt(i));
      sum = 11 - sum % 11;
      return sum == 11 ? '0' : sum == 10 ? 'X' : ("" + sum).charAt(0);
   }
   
   public static char computeCheckDigit13(String EAN){
      if(EAN == null || EAN.length() != 12)
         throw new IllegalArgumentException("The input string must consist of exactly 12 digits."); //else
      
      int sum = 0;
      boolean one = true;
      for(int i=0; i<12; i++){
         sum += (one ? 1 : 3) * Integer.parseInt("" + EAN.charAt(i));
         one = !one;
      }
      sum = 10 - sum % 10;
      return sum == 10 ? '0' : ("" + sum).charAt(0);
   }

}
