package org.books.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBUtils {
   
   public static void printResults(ResultSet results) throws SQLException {
      int columnNumber = results.getMetaData().getColumnCount();
      while ( results.next() ){
         for(int i=1; i<=columnNumber; i++)
            System.out.print(results.getString(i) + "\t");
         System.out.println();
      }
   }
   
   public static void run(File file) throws SQLException, ClassNotFoundException, IOException{
      run(file2string(file, StandardCharsets.UTF_8));
   }
   
   public static void run(String s) throws SQLException, ClassNotFoundException{
      Class.forName("org.hsqldb.jdbcDriver");
      Connection connection = DriverManager.getConnection("jdbc:hsqldb:src/main/resources/wishlist/db", "sa", "");

      Statement statement = connection.createStatement();
      statement.execute(s);

      statement.execute("SHUTDOWN COMPACT");
      connection.close();
   }
   
   public static ResultSet runQuery(File file) throws SQLException, ClassNotFoundException, IOException{
      return runQuery(file2string(file, StandardCharsets.UTF_8));
   }
   
   public static ResultSet runQuery(String s) throws SQLException, ClassNotFoundException{
      Class.forName("org.hsqldb.jdbcDriver");
      Connection connection = DriverManager.getConnection("jdbc:hsqldb:src/main/resources/wishlist/db", "sa", "");

      Statement statement = connection.createStatement();
      ResultSet results = statement.executeQuery(s);
      
      statement.execute("SHUTDOWN COMPACT");
      connection.close();
      
      return results;
   }
   
   private static String file2string(File file, Charset encoding) throws IOException{
      return new String(Files.readAllBytes(file.toPath()), encoding);
   }

}
