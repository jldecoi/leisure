package org.books.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GoogleBooksClient {
   
   private final static String URL = "https://www.googleapis.com/books/v1/volumes";

   public final static int DEFAULT_PORT = 8080;

   private HttpClient      client;
   // Only for optimization.
   private ObjectMapper    mapper;

   public GoogleBooksClient() {
      client = HttpClients.createDefault();
      mapper = new ObjectMapper();
   }
   
   public Map<String, Object> getBook(String isbn) throws IOException{
      Map<String, Object> results = getJson(URL, "q", "isbn:" + isbn);
      if(results == null)
         return null; //else
      
      List<Object> items = (List<Object>)results.get("items");
      if(items == null)
         return null;
      
      int size = items.size();
      switch(size){
      case 0: return null;
      case 1: return doGetBook((String)((Map<String, Object>)items.get(0)).get("selfLink"));
      default: throw new RuntimeException(size + " books have ISBN " + isbn + ".");
      }
   }
   
   private Map<String, Object> doGetBook(String url) throws IOException{
      Map<String, Object> toReturn = getJson(url);
      return toReturn == null ? null : (Map<String, Object>)toReturn.get("volumeInfo");
   }
   
   private Map<String, Object> getJson(String url, String... params) throws IOException{
      try {
         int length = params.length;
         if(length % 2 != 0)
            throw new IllegalArgumentException("As many parameters as values must be provided."); //else
         
         URIBuilder builder = new URIBuilder(url);
         for(int i=0; i<length; i+=2)
            builder.setParameter(params[i], params[i + 1]);
         
         HttpResponse response = client.execute(new HttpGet(builder.build()));
         StatusLine line = response.getStatusLine();
         if ( line.getStatusCode() != HttpStatus.SC_OK )
            throw new IOException(line.getReasonPhrase()); //else

         return mapper.readValue(response.getEntity().getContent(), Map.class);
      }
      catch ( UnsupportedOperationException | URISyntaxException e ) {
         throw new IOException(e);
      }
   }
   
   public static void main( String[] args ) throws IOException, InterruptedException{
      // For all available fields, see https://developers.google.com/books/docs/v1/reference/volumes .
      Set<String> publishers = new HashSet<String>();
      
      for(Map<String, ?> book : (List<Map<String, ?>>)new ObjectMapper().readValue(new File("books.txt"), List.class)){
         String publisher = (String)book.get("publisher");
         if(publisher != null)
            publishers.add(publisher);
      }
      
      for(String publisher : publishers)
         System.out.println(publisher);
   }

}
