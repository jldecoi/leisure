package org.crawler.dm;

import org.junit.Assert;
import org.junit.Test;

public class TTimeFlags {

	@Test
	public void test0() {
		for(TimeInformation information : TimeInformation.values())
			for(boolean b : new boolean[]{ false, true }){
				byte flags = TimeFlags.createFlags(information, b);
				Assert.assertEquals(information, TimeFlags.getTimeInformation(flags));
				Assert.assertEquals(b, TimeFlags.isUnsure(flags));
			}
	}

	@Test
	public void test1() {
		for(TimeInformation information : TimeInformation.values()){
			byte flags = TimeFlags.createFlags(information);
			Assert.assertEquals(information, TimeFlags.getTimeInformation(flags));
			Assert.assertEquals(false, TimeFlags.isUnsure(flags));
		}
	}

	@Test
	public void test2() {
		for(boolean b : new boolean[]{ false, true }){
			byte flags = TimeFlags.createFlags(b);
			Assert.assertNull(TimeFlags.getTimeInformation(flags));
			Assert.assertEquals(b, TimeFlags.isUnsure(flags));
		}
	}

}
