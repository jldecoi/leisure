package org.util;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;

public class CollectionUtils {
   
   public static <X extends Comparable<? super X>> List<X> sort(Set<X> set){
      List<X> toReturn = new ArrayList<X>(set);
      Collections.sort(toReturn);
      return toReturn;
   }
   
   public static <X> List<X> sort(Set<X> set, Comparator<X> comparator){
      List<X> toReturn = new ArrayList<X>(set);
      Collections.sort(toReturn, comparator);
      return toReturn;
   }

   public static byte[] toPrimitiveArray( Byte[] array ) {
      int length = array.length;
      byte[] toReturn = new byte[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static short[] toPrimitiveArray( Short[] array ) {
      int length = array.length;
      short[] toReturn = new short[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static int[] toPrimitiveArray( Integer[] array ) {
      int length = array.length;
      int[] toReturn = new int[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static long[] toPrimitiveArray( Long[] array ) {
      int length = array.length;
      long[] toReturn = new long[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static float[] toPrimitiveArray( Float[] array ) {
      int length = array.length;
      float[] toReturn = new float[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static double[] toPrimitiveArray( Double[] array ) {
      int length = array.length;
      double[] toReturn = new double[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static boolean[] toPrimitiveArray( Boolean[] array ) {
      int length = array.length;
      boolean[] toReturn = new boolean[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static char[] toPrimitiveArray( Character[] array ) {
      int length = array.length;
      char[] toReturn = new char[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Byte[] toObjectArray( byte[] array ) {
      int length = array.length;
      Byte[] toReturn = new Byte[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Short[] toObjectArray( short[] array ) {
      int length = array.length;
      Short[] toReturn = new Short[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Integer[] toObjectArray( int[] array ) {
      int length = array.length;
      Integer[] toReturn = new Integer[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Long[] toObjectArray( long[] array ) {
      int length = array.length;
      Long[] toReturn = new Long[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Float[] toObjectArray( float[] array ) {
      int length = array.length;
      Float[] toReturn = new Float[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Double[] toObjectArray( double[] array ) {
      int length = array.length;
      Double[] toReturn = new Double[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Boolean[] toObjectArray( boolean[] array ) {
      int length = array.length;
      Boolean[] toReturn = new Boolean[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }

   public static Character[] toObjectArray( char[] array ) {
      int length = array.length;
      Character[] toReturn = new Character[length];
      for ( int i = 0; i < length; i++ )
         toReturn[i] = array[i];
      return toReturn;
   }
   
   public static <X, Y> void addToList(X key, Y value, Map<X, List<Y>> map){
      List<Y> collection = map.get(key);
      if(collection != null){
         collection.add(value);
         return;
      } //else
      
      List<Y> toAdd = new ArrayList<Y>();
      toAdd.add(value);
      map.put(key, toAdd);
   }
   
   public static <X, Y> void addToSet(X key, Y value, Map<X, Set<Y>> map){
      Set<Y> collection = map.get(key);
      if(collection != null){
         collection.add(value);
         return;
      } //else
      
      Set<Y> toAdd = new HashSet<Y>();
      toAdd.add(value);
      map.put(key, toAdd);
   }
   
   public static <X> void addOccurrence(X key, Map<X, Integer> map){
      Integer occurrences = map.get(key);
      map.put(key, occurrences == null ? 1 : occurrences + 1);
   }
   
   public static <X> void removeOccurrence(X key, Map<X, Integer> map){
      Integer occurrences = map.get(key);
      if(occurrences == null)
    	  return; //else
      
      if(occurrences == 1)
    	  map.remove(key);
      else map.put(key, occurrences - 1);
   }
   
   public static <X extends Number> double getAverage(Map<X, Integer> map){
	   double toReturn = 0;
	   int occurrences = 0;
	   
	   for(X key : map.keySet()){
		   int value = map.get(key);
		   toReturn += value * key.doubleValue();
		   occurrences += value;
	   }
	   
	   return toReturn / occurrences;
   }
   
   public static List<?> toList(Object... objects){
      List<Object> toReturn = new ArrayList<Object>();
      for(Object object : objects)
         // Unfortunately, the equality of lists containing "Date"s is not as expected.
         if(object instanceof Date)
            toReturn.add(object.toString());
         else toReturn.add(object);
      return toReturn;
   }
   
   public static <X extends Comparable<? super X>> int maxIndex(List<? extends X> list){
       if(list.isEmpty())
           return -1; //else
       
       int index = 0;
       X value = list.get(index);
       for(int i=1; i<list.size(); i++){
           X newValue = list.get(i);
           if(newValue.compareTo(value) <= 0)
               continue; //else
           
           index = i;
           value = newValue;
       }
       
       return index;
   }
   
   public static <X> boolean contains(X[] array, X element){
      for(X x : array)
         if(element == null && x == null || element.equals(x))
            return true; //else
      
      return false;
   }
   
   public static <X> Set<X> intersect(Set<X> set1, Set<X> set2){
	   Set<X> toReturn = new HashSet<X>();
	   
	   for(X x : set1)
		   if(set2.contains(x))
			   toReturn.add(x);
	   
	   return toReturn;
   }

}
