package org.util;

public class OneShotIterator<X> extends AbstractIterator<X> {

	private X next;
	private boolean isFirstTime;
	
	public OneShotIterator(X next){
		this.next = next;
		this.isFirstTime = true;
	}

	@Override
	protected X getNext() {
		if(isFirstTime){
			isFirstTime = false;
			return next;
		} //else
		return null;
	}
	
}
