package org.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class GroupingIterator<DataType> extends AbstractIterator<List<DataType>> {
	
	private Iterator<DataType> inner;
	private DataType next;
	
	public GroupingIterator(Iterator<DataType> inner){
		this.inner = inner;
		next = inner.hasNext() ? inner.next() : null;
	}

	/**
	 * @return <b>NOTE:</b> The returned list is never empty.
	 */
	@Override
	protected List<DataType> getNext() {
		if(next == null)
			return null; //else
		
		List<DataType> toReturn = new ArrayList<DataType>();
		toReturn.add(next);
		
		while(inner.hasNext()){
			DataType next1 = inner.next();
			if(!belongTogether(next, next1)){
				next = next1;
				return toReturn; //else
			}
			toReturn.add(next1);
		}
		
		next = null;
		return toReturn;
	}
	
	/**
	 * <b>NOTE:</b> Implementations must be symmetric. In other words, you have to make sure that, for each <tt>first</tt> and <tt>second</tt>,
	 * <tt>belongTogether(first, second) = belongTogether(second, first)</tt>.
	 * @param first
	 * @param second
	 * @return
	 */
	protected abstract boolean belongTogether(DataType first, DataType second);

}
