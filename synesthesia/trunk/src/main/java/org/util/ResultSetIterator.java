package org.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class ResultSetIterator<DataType> extends AbstractIterator<DataType> {

	private ResultSet results;
	
	public ResultSetIterator(ResultSet results) {
		this.results = results;
	}
	
	@Override
	protected DataType getNext() {
		try {
			return results.next() ? getNext(results) : null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected abstract DataType getNext(ResultSet results) throws SQLException;

}
