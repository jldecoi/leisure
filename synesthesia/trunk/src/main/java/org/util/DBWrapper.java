package org.util;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DBWrapper {

	private static Connection CONNECTION;

	public static synchronized void init() throws ClassNotFoundException, SQLException {
		if (CONNECTION != null)
			throw new IllegalStateException("Connection was already initialized.");

		Class.forName("org.hsqldb.jdbcDriver");
		CONNECTION = DriverManager.getConnection("jdbc:hsqldb:file:src/main/resources/crawler/db", "SA", "");
	}

	public static Statement createStatement() throws SQLException {
		return CONNECTION.createStatement();
	}

	public static PreparedStatement prepareStatement(String sql) throws SQLException {
		return CONNECTION.prepareStatement(sql);
	}

    public static Blob createBlob() throws SQLException {
        return CONNECTION.createBlob();
    }

	public static void close() throws SQLException {
		CONNECTION.createStatement().execute("SHUTDOWN COMPACT");
		CONNECTION.close();
	}

}
