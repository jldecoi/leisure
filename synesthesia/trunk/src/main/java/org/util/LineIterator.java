package org.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


public abstract class LineIterator<X> extends AbstractIterator<X> {
   
   private BufferedReader reader;
   
   public LineIterator(BufferedReader reader){
      this.reader = reader;
   }
   
   public LineIterator(InputStream stream){
      try {
         this.reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
      }
      catch ( UnsupportedEncodingException e ) {
         // It should never occur. Anyway...
         throw new RuntimeException(e);
      }
   }
   
   public LineIterator(File file) throws FileNotFoundException{
      this(new BufferedReader(new FileReader(file)));
   }
   
   public LineIterator(String file) throws FileNotFoundException{
      this(new File(file));
   }

   @Override
   protected X getNext() {
      try {
         String line = reader.readLine();
         return line == null ? null : parseLine(line);
      }
      catch ( IOException e ) {
         throw new RuntimeException(e);
      }
   }
   
   protected abstract X parseLine(String line);

}
