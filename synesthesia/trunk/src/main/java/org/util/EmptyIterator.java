package org.util;

public class EmptyIterator<X> extends AbstractIterator<X> {

	@Override
	protected X getNext() {
		return null;
	}

}
