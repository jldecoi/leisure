package org.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

import org.art.spotify.data.FullArtist;
import org.art.spotify.data.SimplifiedAlbum;
import org.art.spotify.data.SimplifiedTrack;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * TODO Let JAXB generate
 * * customized field names
 * * "hashCode" and "equals" methods
 */
public class SpotifyUtils {
	
	private final static String PREFIX = "https://api.spotify.com/v1/";
	private final static int LIMIT = 50;
	
	private ObjectMapper mapper;
	
	public SpotifyUtils(){
		mapper = new ObjectMapper();
	}
	
	// id name
	public Set<FullArtist> getArtists(String searchString) throws IOException, InterruptedException{
		Set<FullArtist> toReturn = new HashSet<FullArtist>();
		
		int offset = 0;
		JsonNode albums = mapper.readTree(getURL("search?q=" + URLEncoder.encode(searchString, "UTF-8") + "&type=artist&", offset)).get("artists").get("items");
		while(albums.size() == LIMIT){
			for(JsonNode artist : albums)
				toReturn.add(mapper.treeToValue(artist, FullArtist.class));
			
			offset += LIMIT;
			albums = mapper.readTree(getURL("search?q=" + URLEncoder.encode(searchString, "UTF-8") + "&type=artist&", offset)).get("artists").get("items");
		}
		for(JsonNode artist : albums)
			toReturn.add(mapper.treeToValue(artist, FullArtist.class));

		return toReturn;
	}

	// id
	public Set<SimplifiedAlbum> getAlbums(FullArtist artist) throws IOException, InterruptedException{
		return getAlbums(artist.getId());
	}

	// id
	public Set<SimplifiedAlbum> getAlbums(String artist) throws IOException, InterruptedException{
		Set<SimplifiedAlbum> toReturn = new HashSet<SimplifiedAlbum>();
		
		int offset = 0;
		JsonNode albums = mapper.readTree(getURL("artists/" + artist + "/albums?", offset)).get("items");
		while(albums.size() == LIMIT){
			for(JsonNode album : albums)
				toReturn.add(mapper.treeToValue(album, SimplifiedAlbum.class));
			
			offset += LIMIT;
			albums = mapper.readTree(getURL("artists/" + artist + "/albums?", offset)).get("items");
		}
		for(JsonNode album : albums)
			toReturn.add(mapper.treeToValue(album, SimplifiedAlbum.class));
		
		return toReturn;
	}

	// artists duration_ms id name
	// https://play.spotify.com/album/<TRACK_ID>
	public Set<SimplifiedTrack> getTracks(SimplifiedAlbum album) throws IOException, InterruptedException{
		return getTracks(album.getId());
	}

	// artists duration_ms id name
	// https://play.spotify.com/album/<TRACK_ID>
	public Set<SimplifiedTrack> getTracks(String album) throws IOException, InterruptedException{
		Set<SimplifiedTrack> toReturn = new HashSet<SimplifiedTrack>();
		
		int offset = 0;
		JsonNode tracks = mapper.readTree(getURL("albums/" + album + "/tracks?", offset)).get("items");
		while(tracks.size() == LIMIT){
			for(JsonNode track : tracks)
				toReturn.add(mapper.treeToValue(track, SimplifiedTrack.class));
			
			offset += LIMIT;
			tracks = mapper.readTree(getURL("albums/" + album + "/tracks?", offset)).get("items");
		}
		for(JsonNode track : tracks)
			toReturn.add(mapper.treeToValue(track, SimplifiedTrack.class));
		
		return toReturn;
	}
	
	private URL getURL(String s, int offset) throws MalformedURLException, InterruptedException{
		URL toReturn = new URL(PREFIX + s + "limit=" + LIMIT + "&offset=" + offset);
		Thread.sleep(1000);
		return toReturn;
	}

}
