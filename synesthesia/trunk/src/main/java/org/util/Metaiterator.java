package org.util;

import java.util.Arrays;
import java.util.Iterator;

public class Metaiterator<X> extends AbstractIterator<X> {
	
	private Iterator<Iterator<X>> iterators;
	private Iterator<X> currentIterator;
	
	public Metaiterator(Iterator<X>... iterators){
		this.iterators = Arrays.asList(iterators).iterator();
	}

	@Override
	protected X getNext() {
		// "currentIterator == null" holds only the first time.
		while(currentIterator == null || !currentIterator.hasNext()){
			if(!iterators.hasNext())
				return null; //else
			currentIterator = iterators.next();
		}
		return currentIterator.next();
	}

}
