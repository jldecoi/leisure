package org.crawler.dm;

public class TimeFlags {
	
	private final static byte MASK = (byte)(1 << 7);
	
	public static byte createFlags(TimeInformation information, boolean unsure){
		byte toReturn;
		
		if(information == null)
			toReturn = 0;
		else switch(information){
		case BEFORE: toReturn = 1; break;
		case AFTER: toReturn = 2; break;
		case CIRCA: toReturn = 3; break;
		case AROUND: toReturn = 4; break;
		default: throw new IllegalArgumentException("Time information '" + information + "' is not supported.");
		}
		
		return unsure ? (byte)(toReturn | MASK) : toReturn;
	}
	
	public static byte createFlags(TimeInformation information){
		return createFlags(information, false);
	}
	
	public static byte createFlags(boolean unsure){
		return createFlags(null, unsure);
	}
	
	public static TimeInformation getTimeInformation(byte b){
		switch(b & ~MASK){
		case 0: return null;
		case 1: return TimeInformation.BEFORE;
		case 2: return TimeInformation.AFTER;
		case 3: return TimeInformation.CIRCA;
		case 4: return TimeInformation.AROUND;
		default: throw new IllegalArgumentException("Byte " + b + " is not a valid time flag.");		
		}
	}
	
	public static boolean isUnsure(byte b){
		return (byte)(b & MASK) != 0;
	}

}
