package org.crawler.dm;

public enum TimeInformation {
	BEFORE("before "),
	AFTER("after "),
	CIRCA("ca. ", "c. "),
	AROUND("around ");

	public static TimeInformation parse(String s) {
		for (TimeInformation flag : TimeInformation.values())
			for (String value : flag.values)
				if (value.equals(s))
					return flag; // else

		throw new IllegalArgumentException("String '" + s + "' does not represent any valid TimeInformation.");
	}

	private String[] values;

	private TimeInformation(String... s) {
		values = s;
	}

	public String toString() {
		return values[0];
	}
	
}
