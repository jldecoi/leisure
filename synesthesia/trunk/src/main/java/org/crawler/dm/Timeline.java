package org.crawler.dm;

import java.text.DecimalFormat;

public enum Timeline {
	
   VIII_1(7, true),
   VIII_2(7, false),
   IX_1(8, true),
   IX_2(8, false),
   X_1(9, true),
   X_2(9, false),
   XI_1(10, true),
   XI_2(10, false),
   XII_1(11, true),
   XII_2(11, false),
   XIII_1(12, true),
   XIII_2(12, false),
   XIV_1(13, true),
   XIV_2(13, false),
   XV_1(14, true),
   XV_2(14, false),
   XVI_1(15, true),
   XVI_2(15, false),
   XVII_1(16, true),
   XVII_2(16, false),
   XVIII_1(17, true),
   XVIII_2(17, false),
   XIX_1(18, true),
   XIX_2(18, false);
   
   public static Timeline parse(String s){
      for(Timeline timeFrame : Timeline.values())
         if(timeFrame.toString().equals(s))
            return timeFrame; //else
      
      throw new IllegalArgumentException("String '" + s + "' does not represent any valid time frame.");
   }
   
   private int century;
   private boolean firstHalf;
   
   private Timeline(int century, boolean firstHalf){
      this.century = century;
      this.firstHalf = firstHalf;
   }
   
   public String toString(){
	  DecimalFormat format = new DecimalFormat("0000");
	  
      int toReturn = 100 * century;
      if(firstHalf)
         return format.format(toReturn + 1) + "-" + format.format(toReturn + 50);
      else return format.format(toReturn + 51) + "-" + format.format(toReturn + 100);
   }

}
