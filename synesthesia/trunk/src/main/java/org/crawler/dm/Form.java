package org.crawler.dm;


public enum Form {

   ARCHITECTURE("architecture"),
   CERAMICS("ceramics"),
   FURNITURE("furniture"),
   GLASSWARE("glassware"),
   GRAPHICS("graphics"),
   ILLUMINATION("illumination"),
   METALWORK("metalwork"),
   MOSAIC("mosaic"),
   OTHERS("others"),
   PAINTING("painting"),
   SCULPTURE("sculpture"),
   STAINED_GLASS("stained-glass"),
   TAPESTRY("tapestry");
   
   public static Form parse(String s){
      for(Form form : Form.values())
         if(form.toString().equals(s))
            return form; //else
      
      throw new IllegalArgumentException("String '" + s + "' does not represent any valid form.");
   }
   
   private String value;
   
   private Form(String s){
      value = s;
   }
   
   public String toString(){
      return value;
   }
   
}
