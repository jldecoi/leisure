package org.crawler.dm;


public enum School {
   
   AMERICAN("American"),
   AUSTRIAN("Austrian"),
   BELGIAN("Belgian"),
   BOHEMIAN("Bohemian"),
   CATALAN("Catalan"),
   DANISH("Danish"),
   DUTCH("Dutch"),
   ENGLISH("English"),
   FINNISH("Finnish"),
   FLEMISH("Flemish"),
   FRENCH("French"),
   GERMAN("German"),
   GREEK("Greek"),
   HUNGARIAN("Hungarian"),
   IRISH("Irish"),
   ITALIAN("Italian"),
   NETHERLANDISH("Netherlandish"),
   NORWEGIAN("Norwegian"),
   OTHER("Other"),
   POLISH("Polish"),
   PORTUGUESE("Portuguese"),
   RUSSIAN("Russian"),
   SCOTTISH("Scottish"),
   SPANISH("Spanish"),
   SWEDISH("Swedish"),
   SWISS("Swiss");
   
   public static School parse(String s){
      for(School school : School.values())
         if(school.toString().equals(s))
            return school; //else
      
      throw new IllegalArgumentException("String '" + s + "' does not represent any valid school.");
   }
   
   private String value;
   
   private School(String s){
      value = s;
   }
   
   public String toString(){
      return value;
   }

}
