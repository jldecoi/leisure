package org.crawler.dm;


public enum Type {
   
   GENRE("genre"),
   HISTORICAL("historical"),
   INTERIOR("interior"),
   LANDSCAPE("landscape"),
   MYTHOLOGICAL("mythological"),
   OTHER("other"),
   PORTRAIT("portrait"),
   RELIGIOUS("religious"),
   STILL_LIFE("still-life"),
   STUDY("study");
   
   public static Type parse(String s){
      for(Type type : Type.values())
         if(type.toString().equals(s))
            return type; //else
      
      throw new IllegalArgumentException("String '" + s + "' does not represent any valid type.");
   }
   
   private String value;
   
   private Type(String s){
      value = s;
   }
   
   public String toString(){
      return value;
   }

}
