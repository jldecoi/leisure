package org.crawler.dm;

public enum PeriodType {
	ACTIVE("active"), DOCUMENTED("documented", "doc."), FLORUIT("fl.");

	public static PeriodType parse(String s) {
		for (PeriodType flag : PeriodType.values())
			for (String value : flag.values)
				if (value.equals(s))
					return flag; // else

		throw new IllegalArgumentException("String '" + s + "' does not represent any valid PeriodType.");
	}

	private String[] values;

	private PeriodType(String... s) {
		values = s;
	}

	public String toString() {
		return values[0];
	}

}
