package org.crawler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;

public interface IStorer<X> {

	public void store(Iterator<X> entries) throws SQLException, IOException;

}
