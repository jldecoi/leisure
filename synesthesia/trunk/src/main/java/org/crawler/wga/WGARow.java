package org.crawler.wga;

public class WGARow {
	
	private String author;
	private String bornDied;
	private String title;
	private String date;
	private String technique;
	private String location;
	private String url;
	private String form;
	private String type;
	private String school;
	private String timeline;

	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getBornDied() {
		return bornDied;
	}
	
	public void setBornDied(String bornDied) {
		this.bornDied = bornDied;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTechnique() {
		return technique;
	}
	
	public void setTechnique(String technique) {
		this.technique = technique;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getForm() {
		return form;
	}
	
	public void setForm(String form) {
		this.form = form;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSchool() {
		return school;
	}
	
	public void setSchool(String school) {
		this.school = school;
	}
	
	public String getTimeline() {
		return timeline;
	}
	
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}

}
