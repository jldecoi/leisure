package org.crawler.wga;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.crawler.AbstractStorer;
import org.crawler.dm.PeriodType;

public class WGAStorer extends AbstractStorer<Entry<WGAAuthor, List<WGAWork>>> {
	
	public WGAStorer() throws SQLException, IOException{
		super(new String[]{ "dropWorks.sql", "dropAuthor.sql", "createAuthors.sql", "createWorks.sql" }, new String[]{ "insertIntoAuthors.sql", "insertIntoWorks.sql" });
	}
	
	@Override
	public void store(Iterator<Entry<WGAAuthor, List<WGAWork>>> entries) throws SQLException, IOException {
		PreparedStatement authors = getPreparedStatement(0);
		PreparedStatement works = getPreparedStatement(1);
		int id = 0;
		
		while(entries.hasNext()){
			Entry<WGAAuthor, List<WGAWork>> entry = entries.next();
			
			WGAAuthor author = entry.getKey();
			Short birthyear = author.getBirthyear();
			Byte birthyearFlags = author.getBirthyearFlags();
			String birthplace = author.getBirthplace();
			PeriodType periodType = author.getPeriodType();
			Short periodBegin = author.getPeriodBegin();
			Short periodEnd = author.getPeriodEnd();
			Byte periodFlags = author.getPeriodFlags();
			String periodPlace = author.getPeriodPlace();
			Short deathyear = author.getDeathyear();
			Byte deathyearFlags = author.getDeathyearFlags();
			String deathplace = author.getDeathplace();
			
			authors.setString(1, author.getName());
			if(birthyear != null)
				authors.setShort(2, birthyear);
			else authors.setNull(2, Types.SMALLINT);
			if(birthyearFlags != null)
				authors.setByte(3, birthyearFlags);
			else authors.setNull(3, Types.TINYINT);
			if(birthplace != null)
				authors.setString(4, birthplace);
			else authors.setNull(4, Types.VARCHAR);
			if(periodType != null)
				authors.setByte(5, (byte)periodType.ordinal());
			else authors.setNull(5, Types.TINYINT);
			if(periodBegin != null)
				authors.setShort(6, periodBegin);
			else authors.setNull(6, Types.SMALLINT);
			if(periodEnd != null)
				authors.setShort(7, periodEnd);
			else authors.setNull(7, Types.SMALLINT);
			if(periodFlags != null)
				authors.setByte(8, periodFlags);
			else authors.setNull(8, Types.TINYINT);
			if(periodPlace != null)
				authors.setString(9, periodPlace);
			else authors.setNull(9, Types.VARCHAR);
			if(deathyear != null)
				authors.setShort(10, deathyear);
			else authors.setNull(10, Types.SMALLINT);
			if(deathyearFlags != null)
				authors.setByte(11, deathyearFlags);
			else authors.setNull(11, Types.TINYINT);
			if(deathplace != null)
				authors.setString(12, deathplace);
			else authors.setNull(12, Types.VARCHAR);
			authors.setByte(13, (byte)author.getSchool().ordinal());
			authors.setByte(14, (byte)author.getTimeline().ordinal());
			authors.execute();
			
			for(WGAWork work : entry.getValue()){
				works.setInt(1, id);
				works.setString(2, work.getTitle());
				works.setString(3, work.getDate());
				works.setString(4, work.getTechnique());
				works.setString(5, work.getLocation());
				works.setString(6, work.getUrl().toString());
				works.setByte(7, (byte)work.getForm().ordinal());
				works.setByte(8, (byte)work.getType().ordinal());
				works.execute();
			}
			
			id++;
		}
	}

}
