package org.crawler.wga;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.ZipInputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.crawler.IRetriever;

public class WGARetriever implements IRetriever<Entry<WGAAuthor, List<WGAWork>>> {
	
	private final static String URL = "http://www.wga.hu/database/download/data_xls.zip";
	
	@Override
	public WGAIterator retrieve() throws IOException {
		ZipInputStream stream = new ZipInputStream(new URL(URL).openStream());
		stream.getNextEntry();
		return new WGAIterator(new HSSFWorkbook(stream));
	}

}
