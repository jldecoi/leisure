package org.crawler.wga;

import java.net.URI;

import org.crawler.dm.Form;
import org.crawler.dm.Type;

public class WGAWork {
	
	private String title;
	private String date;
	private String technique;
	private String location;
	private URI url;
	private Form form;
	private Type type;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTechnique() {
		return technique;
	}
	
	public void setTechnique(String technique) {
		this.technique = technique;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public URI getUrl() {
		return url;
	}
	
	public void setUrl(URI url) {
		this.url = url;
	}
	
	public Form getForm() {
		return form;
	}
	
	public void setForm(Form form) {
		this.form = form;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}

}
