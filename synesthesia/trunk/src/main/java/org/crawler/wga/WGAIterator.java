package org.crawler.wga;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.crawler.dm.Form;
import org.crawler.dm.PeriodType;
import org.crawler.dm.School;
import org.crawler.dm.TimeFlags;
import org.crawler.dm.TimeInformation;
import org.crawler.dm.Timeline;
import org.crawler.dm.Type;
import org.util.AbstractIterator;
import org.util.FilterIterator;
import org.util.MappingIterator;
import org.util.MergingIterator;

public class WGAIterator extends AbstractIterator<Entry<WGAAuthor, List<WGAWork>>> implements Closeable {

	private final static String[] HEADER = { "AUTHOR", "BORN-DIED", "TITLE", "DATE", "TECHNIQUE", "LOCATION", "URL",
			"FORM", "TYPE", "SCHOOL", "TIMELINE" };
	private final static Entry<WGAAuthor, List<WGAWork>> NULL_ENTRY = new SimpleEntry<WGAAuthor, List<WGAWork>>(null,
			null);

	private static final Logger LOGGER = LogManager.getLogger(WGAIterator.class);

	private HSSFWorkbook workbook;
	private Iterator<Entry<WGAAuthor, List<WGAWork>>> inner;

	public WGAIterator(HSSFWorkbook workbook) {
		this.workbook = workbook;
		Iterator<Row> iterator = workbook.getSheetAt(0).rowIterator();

		checkHeader(iterator.next());
		inner = new FilterIterator<Entry<WGAAuthor, List<WGAWork>>>(
				new MergingIterator<WGARow, Entry<WGAAuthor, List<WGAWork>>>(
						new MappingIterator<Row, WGARow>(iterator) {

							@Override
							protected WGARow map(Row row) {
								WGARow toReturn = new WGARow();
								toReturn.setAuthor(getCell(row, 0));
								toReturn.setBornDied(getCell(row, 1));
								toReturn.setTitle(getCell(row, 2));
								toReturn.setDate(getCell(row, 3));
								toReturn.setTechnique(getCell(row, 4));
								toReturn.setLocation(getCell(row, 5));
								toReturn.setUrl(getCell(row, 6));
								toReturn.setForm(getCell(row, 7));
								toReturn.setType(getCell(row, 8));
								toReturn.setSchool(getCell(row, 9));
								toReturn.setTimeline(getCell(row, 10));
								return toReturn;
							}

							private String getCell(Row row, int index) {
								Cell cell = row.getCell(index);
								return cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? "" + cell.getNumericCellValue()
										: cell.getStringCellValue();
							}

						}) {

					@Override
					protected boolean belongTogether(WGARow first, WGARow second) {
						return first.getAuthor().equals(second.getAuthor());
					}

					@Override
					protected Entry<WGAAuthor, List<WGAWork>> merge(List<WGARow> list) {
						// "list" is never empty.
						WGARow first = list.get(0);
						String name = first.getAuthor();

						Set<String> bornDieds = new HashSet<String>();
						Set<String> schools = new HashSet<String>();
						Set<String> timelines = new HashSet<String>();
						for (WGARow row : list) {
							bornDieds.add(row.getBornDied());
							schools.add(row.getSchool());
							timelines.add(row.getTimeline());
						}
						if (bornDieds.size() != 1) {
							LOGGER.warn("Multiple 'bornDied' values specified for author '" + name + "': " + bornDieds
									+ ".");
							return NULL_ENTRY;
						}
						if (schools.size() != 1) {
							LOGGER.warn(
									"Multiple 'school' values specified for author '" + name + "': " + schools + ".");
							return NULL_ENTRY;
						}
						if (timelines.size() != 1) {
							LOGGER.warn("Multiple 'timeline' values specified for author '" + name + "': " + timelines
									+ ".");
							return NULL_ENTRY;
						}

						WGAAuthor author = new WGAAuthor();

						name = first.getAuthor();
						String bornDied = first.getBornDied();
						String school = first.getSchool();
						String timeline = first.getTimeline();

						if (!checkAuthor(author, name)) {
							LOGGER.warn("Wrong field 'author' for author '" + name + "': '" + name + "'.");
							return NULL_ENTRY;
						}
						if (!checkBornDied(author, bornDied)) {
							LOGGER.warn("Wrong field 'bornDied' for author '" + name + "': '" + bornDied + "'.");
							return NULL_ENTRY;
						}
						if (!checkSchool(author, school)) {
							LOGGER.warn("Wrong field 'school' for author '" + name + "': '" + school + "'.");
							return NULL_ENTRY;
						}
						if (!checkTimeline(author, timeline)) {
							LOGGER.warn("Wrong field 'timeline' for author '" + name + "': '" + timeline + "'.");
							return NULL_ENTRY;
						}

						List<WGAWork> works = new ArrayList<WGAWork>();
						for (WGARow row : list) {
							WGAWork work = new WGAWork();

							String title = row.getTitle();
							String date = row.getDate();
							String technique = row.getTechnique();
							String location = row.getLocation();
							String url = row.getUrl();
							String form = row.getForm();
							String type = row.getType();

							if (!checkTitle(work, title)) {
								LOGGER.warn("Wrong field 'title' for author '" + name + "': '" + title + "'.");
								continue;
							}
							if (!checkDate(work, date)) {
								LOGGER.warn("Wrong field 'date' for author '" + name + "': '" + date + "'.");
								continue;
							}
							if (!checkTechnique(work, technique)) {
								LOGGER.warn("Wrong field 'technique' for author '" + name + "': '" + technique + "'.");
								continue;
							}
							if (!checkLocation(work, location)) {
								LOGGER.warn("Wrong field 'location' for author '" + name + "': '" + location + "'.");
								continue;
							}
							if (!checkUrl(work, url)) {
								LOGGER.warn("Wrong field 'url' for author '" + name + "': '" + url + "'.");
								continue;
							}
							if (!checkForm(work, form)) {
								LOGGER.warn("Wrong field 'form' for author '" + name + "': '" + form + "'.");
								continue;
							}
							if (!checkType(work, type)) {
								LOGGER.warn("Wrong field 'type' for author '" + name + "': '" + type + "'.");
								continue;
							}

							works.add(work);
						}

						if (works.isEmpty()) {
							LOGGER.warn("No well-formed works for author '" + name + "'.");
							return NULL_ENTRY;
						}

						return new SimpleEntry<WGAAuthor, List<WGAWork>>(author, works);
					}

					private boolean checkAuthor(WGAAuthor author, String name) {
						/*
						 * if(name.split(", ").length != 2) return false;
						 */

						author.setName(name);
						return true;
					}

					private boolean checkBornDied(WGAAuthor author, String bornDied) {
						Pattern p1 = Pattern.compile(
								"\\(b\\. (ca\\. |before |after )?(\\d{1,4})(, [^,\\)]+(, [^,\\)]+)?)?, d\\. (ca\\. |before |after )?(\\d{1,4})(, [^,\\)]+(, [^,\\)]+)?)?\\)");
						Matcher m1 = p1.matcher(bornDied);
						if (m1.matches()) {
							String group1 = m1.group(1);
							String group3 = m1.group(3);
							String group5 = m1.group(5);
							String group7 = m1.group(7);

							if (group1 != null)
								author.setBirthyearFlags(TimeFlags.createFlags(TimeInformation.parse(group1)));
							author.setBirthyear(Short.parseShort(m1.group(2)));
							if (group3 != null)
								author.setBirthplace(group3.substring(2));
							if (group5 != null)
								author.setDeathyearFlags(TimeFlags.createFlags(TimeInformation.parse(group5)));
							author.setDeathyear(Short.parseShort(m1.group(6)));
							if (group7 != null)
								author.setDeathplace(group7.substring(2));

							return true;
						}

						Pattern p2 = Pattern.compile(
								"\\((active|doc\\.|documented) (around |ca\\. |c\\. |after )?(\\d{1,4})(s|-(\\d{1,4}))?( in ([^\\)]+))?\\)");
						Matcher m2 = p2.matcher(bornDied);
						if (m2.matches()) {
							String group2 = m2.group(2);
							String group4 = m2.group(4);
							String group5 = m2.group(5);
							String group7 = m2.group(7);

							author.setPeriodType(PeriodType.parse(m2.group(1)));
							if (group2 != null)
								author.setPeriodFlags(TimeFlags.createFlags(TimeInformation.parse(group2)));
							short group3 = Short.parseShort(m2.group(3));
							author.setPeriodBegin(group3);
							if (group4 == null)
								author.setPeriodEnd(group3);
							else if (group5 == null)
								author.setPeriodEnd((short)(group3 + 9));
							else
								author.setPeriodEnd(Short.parseShort(group5));
							if (group7 != null)
								author.setPeriodPlace(group7);

							return true;
						}

						return false;
					}

					private boolean checkSchool(WGAAuthor author, String school) {
						try {
							author.setSchool(School.parse(school));
							return true;
						} catch (IllegalArgumentException e) {
							return false;
						}
					}

					private boolean checkTimeline(WGAAuthor author, String timeline) {
						try {
							author.setTimeline(Timeline.parse(timeline));
							return true;
						} catch (IllegalArgumentException e) {
							return false;
						}
					}

					private boolean checkTitle(WGAWork work, String title) {
						work.setTitle(title);
						return true;
					}

					private boolean checkDate(WGAWork work, String date) {
						/*
						 * if (!(value.
						 * matches("(c[\\.\\-,]? ?)?(\\d*)((-|�| or )(\\d*))?( ?\\.)?( \\(.*\\))?"
						 * ) || value.
						 * matches("(before|around|begun|after|about) (c\\. )?(\\d*)"
						 * ) || value.matches("(\\d*)( \\(\\?\\)|'?s)"))) return
						 * false;
						 */

						work.setDate(date);
						return true;
					}

					private boolean checkTechnique(WGAWork work, String technique) {
						/*
						 * if(!technique.
						 * matches("(.*), (.*) x (.*) (..)( \\(.*\\))?") && !
						 * technique.
						 * matches("(.*), height (.*) (..)( \\(.*\\))?")) return
						 * false;
						 */

						work.setTechnique(technique);
						return true;
					}

					private boolean checkLocation(WGAWork work, String location) {
						/*
						 * if(location.split(", ").length != 2) return false;
						 */

						work.setLocation(location);
						return true;
					}

					private boolean checkUrl(WGAWork work, String url) {
						try {
							work.setUrl(new URI("http://www.wga.hu/art/"
									+ url.substring("http://www.wga.hu/html/".length(), url.length() - ".html".length())
									+ ".jpg"));
							return true;
						} catch (URISyntaxException e) {
							return false;
						}
					}

					private boolean checkForm(WGAWork work, String form) {
						try {
							work.setForm(Form.parse(form));
							return true;
						} catch (IllegalArgumentException e) {
							return false;
						}
					}

					private boolean checkType(WGAWork work, String type) {
						try {
							work.setType(Type.parse(type));
							return true;
						} catch (IllegalArgumentException e) {
							return false;
						}
					}

				}) {

			@Override
			protected boolean filterOut(Entry<WGAAuthor, List<WGAWork>> element) {
				return element == NULL_ENTRY;
			}

		};
	}

	private void checkHeader(Row row) {
		int length = HEADER.length;
		if (row.getFirstCellNum() != 0 || row.getLastCellNum() != HEADER.length)
			throw new IllegalArgumentException();

		for (int i = 0; i < length; i++)
			if (!HEADER[i].equals(row.getCell(i).getStringCellValue()))
				throw new IllegalArgumentException();
	}

	@Override
	protected Entry<WGAAuthor, List<WGAWork>> getNext() {
		return inner.hasNext() ? inner.next() : null;
	}

	@Override
	public void close() throws IOException {
		workbook.close();
	}

}
