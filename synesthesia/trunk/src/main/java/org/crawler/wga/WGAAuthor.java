package org.crawler.wga;

import org.crawler.dm.PeriodType;
import org.crawler.dm.School;
import org.crawler.dm.Timeline;

public class WGAAuthor {
	
	private String name;
	private Short birthyear;
	private Byte birthyearFlags;
	private String birthplace;
	private PeriodType periodType;
	private Short periodBegin;
	private Short periodEnd;
	private Byte periodFlags;
	private String periodPlace;
	private Short deathyear;
	private Byte deathyearFlags;
	private String deathplace;
	private School school;
	private Timeline timeline;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Short getBirthyear() {
		return birthyear;
	}
	
	public void setBirthyear(Short birthyear) {
		this.birthyear = birthyear;
	}
	
	public Byte getBirthyearFlags() {
		return birthyearFlags;
	}
	
	public void setBirthyearFlags(Byte birthyearFlags) {
		this.birthyearFlags = birthyearFlags;
	}
	
	public String getBirthplace() {
		return birthplace;
	}
	
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	
	public PeriodType getPeriodType() {
		return periodType;
	}
	
	public void setPeriodType(PeriodType periodType) {
		this.periodType = periodType;
	}
	
	public Short getPeriodBegin() {
		return periodBegin;
	}
	
	public void setPeriodBegin(Short periodBegin) {
		this.periodBegin = periodBegin;
	}
	
	public Short getPeriodEnd() {
		return periodEnd;
	}
	
	public void setPeriodEnd(Short periodEnd) {
		this.periodEnd = periodEnd;
	}
	
	public Byte getPeriodFlags() {
		return periodFlags;
	}
	
	public void setPeriodFlags(Byte periodFlags) {
		this.periodFlags = periodFlags;
	}
	
	public String getPeriodPlace() {
		return periodPlace;
	}
	
	public void setPeriodPlace(String periodPlace) {
		this.periodPlace = periodPlace;
	}
	
	public Short getDeathyear() {
		return deathyear;
	}
	
	public void setDeathyear(Short deathyear) {
		this.deathyear = deathyear;
	}
	
	public Byte getDeathyearFlags() {
		return deathyearFlags;
	}
	
	public void setDeathyearFlags(Byte deathyearFlags) {
		this.deathyearFlags = deathyearFlags;
	}
	
	public String getDeathplace() {
		return deathplace;
	}
	
	public void setDeathplace(String deathplace) {
		this.deathplace = deathplace;
	}
	
	public School getSchool() {
		return school;
	}
	
	public void setSchool(School school) {
		this.school = school;
	}
	
	public Timeline getTimeline() {
		return timeline;
	}
	
	public void setTimeline(Timeline timeline) {
		this.timeline = timeline;
	}

}
