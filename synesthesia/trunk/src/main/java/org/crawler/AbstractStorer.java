package org.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.util.DBWrapper;

public abstract class AbstractStorer<X> implements IStorer<X> {
	
	private final static String PATH = "src/main/resources/sql/";
	
	private static String fromFile(String file) throws IOException{
		return new String(Files.readAllBytes(Paths.get(PATH + file))); 
	}
	
	private PreparedStatement[] preparedStatements;
	
	public AbstractStorer(String[] toExecute, String[] toPrepare) throws SQLException, IOException {
		Statement statement = DBWrapper.createStatement();
		for(String s : toExecute)
			statement.execute(fromFile(s));
		
		int length = toPrepare.length;
		preparedStatements = new PreparedStatement[length];
		for(int i=0; i<length; i++)
			preparedStatements[i] = DBWrapper.prepareStatement(fromFile(toPrepare[i]));
	}
	
	protected PreparedStatement getPreparedStatement(int index){
		return preparedStatements[index];
	}

}
