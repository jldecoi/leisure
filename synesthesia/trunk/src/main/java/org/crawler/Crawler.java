package org.crawler;

import java.io.IOException;
import java.sql.SQLException;

public class Crawler<X> {
	
	private IRetriever<X> retriever;
	private IStorer<X> storer;
	
	public Crawler(IRetriever<X> retriever, IStorer<X> storer){
		this.retriever = retriever;
		this.storer = storer;
	}
	
	public void crawl() throws IOException, SQLException{
		storer.store(retriever.retrieve());
	}

}
