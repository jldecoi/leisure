package org.crawler.wikipedia;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;

import org.crawler.AbstractStorer;
import org.crawler.dm.PeriodType;

public class WikipediaStorer extends AbstractStorer<WikipediaComposer> {

	public WikipediaStorer()
			throws SQLException, IOException {
		super(new String[]{ "dropComposerNames.sql", "dropComposers.sql", "createComposers.sql", "createComposerNames.sql" }, new String[]{ "insertIntoComposers.sql", "insertIntoComposerNames.sql" });
	}

	@Override
	public void store(Iterator<WikipediaComposer> entries) throws SQLException, IOException {
		PreparedStatement composers = getPreparedStatement(0);
		PreparedStatement composerNames = getPreparedStatement(1);
		int id = 0;
		
		while(entries.hasNext()){
			WikipediaComposer composer = entries.next();
			
			String alternativeName = composer.getAlternativeName();
			Short birthyear = composer.getBirthyear();
			Byte birthyearFlags = composer.getBirthyearFlags();
			PeriodType periodType = composer.getPeriodType();
			Short periodBegin = composer.getPeriodBegin();
			Byte periodBeginFlags = composer.getPeriodBeginFlags();
			Short periodEnd = composer.getPeriodEnd();
			Byte periodEndFlags = composer.getPeriodEndFlags();
			Short deathyear = composer.getDeathyear();
			Byte deathyearFlags = composer.getDeathyearFlags();

			composers.setString(1, composer.getHref());
			composers.setString(2, composer.getTitle());
			if(birthyear != null)
				composers.setShort(3, birthyear);
			else composers.setNull(3, Types.SMALLINT);
			if(birthyearFlags != null)
				composers.setByte(4, birthyearFlags);
			else composers.setNull(4, Types.TINYINT);
			if(periodType != null)
				composers.setByte(5, (byte)periodType.ordinal());
			else composers.setNull(5, Types.TINYINT);
			if(periodBegin != null)
				composers.setShort(6, periodBegin);
			else composers.setNull(6, Types.SMALLINT);
			if(periodBeginFlags != null)
				composers.setByte(7, periodBeginFlags);
			else composers.setNull(7, Types.TINYINT);
			if(periodEnd != null)
				composers.setShort(8, periodEnd);
			else composers.setNull(8, Types.SMALLINT);
			if(periodEndFlags != null)
				composers.setByte(9, periodEndFlags);
			else composers.setNull(9, Types.TINYINT);
			if(deathyear != null)
				composers.setShort(10, deathyear);
			else composers.setNull(10, Types.SMALLINT);
			if(deathyearFlags != null)
				composers.setByte(11, deathyearFlags);
			else composers.setNull(11, Types.TINYINT);
			composers.execute();
			
			composerNames.setInt(1, id);
			composerNames.setString(2, composer.getName());
			composerNames.execute();
			
			if(alternativeName != null){
				composerNames.setInt(1, id);
				composerNames.setString(2, alternativeName);
				composerNames.execute();
			}
			
			id++;
		}
	}

}
