package org.crawler.wikipedia;

public class WikipediaRow {

	private String href;
	private String title;
	private String name;
	private String alternativeName;
	private String lifeSpan;
	
	public String getHref() {
		return href;
	}
	
	public void setHref(String href) {
		this.href = href;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAlternativeName() {
		return alternativeName;
	}
	
	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}
	
	public String getLifeSpan() {
		return lifeSpan;
	}
	
	public void setLifeSpan(String lifeSpan) {
		this.lifeSpan = lifeSpan;
	}

}
