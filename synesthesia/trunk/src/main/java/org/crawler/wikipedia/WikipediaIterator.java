package org.crawler.wikipedia;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.crawler.dm.PeriodType;
import org.crawler.dm.TimeFlags;
import org.crawler.dm.TimeInformation;
import org.util.FilterIterator;
import org.util.MappingIterator;

public class WikipediaIterator extends FilterIterator<WikipediaComposer> {
	
	private final static Pattern P1_1 = Pattern.compile("\\Q<li><a href=\"\\E([^\"]*)\\Q\" title=\"\\E([^\"]*)\\Q\">\\E([^<]*)\\Q</a> (\\E([^\\)]*)\\Q)</li>\\E");
	private final static Pattern P2_1 = Pattern.compile("\\Q<li><a href=\"\\E([^\"]*)\\Q\" title=\"\\E([^\"]*)\\Q\">\\E([^<]*)\\Q</a> (\\E([^\\)]*)\\Q) (\\E([^\\)]*)\\Q)</li>\\E");

	private final static Pattern P1_2 = Pattern.compile("(fl\\. )?(c\\. |before |after )?(\\d{1,4})( BC)?(\\?)?–(c\\. |before |after )?(\\d{1,4})( BC)?(\\?)?");
	private final static Pattern P2_2 = Pattern.compile("(born|died) (c\\. |before |after )?(\\d{1,4})( BC)?(\\?)?");
	
	private final static WikipediaRow NULL_ENTRY_1 = new WikipediaRow();
	private final static WikipediaComposer NULL_ENTRY_2 = new WikipediaComposer();

	private static final Logger LOGGER = LogManager.getLogger(WikipediaIterator.class);

	public WikipediaIterator(Iterator<String> inner){
		super(new MappingIterator<WikipediaRow, WikipediaComposer>(new FilterIterator<WikipediaRow>(new MappingIterator<String, WikipediaRow>(new FilterIterator<String>(inner){
			
			@Override
			protected boolean filterOut(String element) {
				return !element.startsWith("<li>");
			}
			
		}){

			@Override
			protected WikipediaRow map(String line) {
				Matcher m1 = P1_1.matcher(line);
				if(m1.matches()){
					WikipediaRow row = new WikipediaRow();
					row.setHref(m1.group(1));
					row.setTitle(m1.group(2));
					row.setName(m1.group(3));
					row.setLifeSpan(m1.group(4));
					return row;
				}
				
				Matcher m2 = P2_1.matcher(line);
				if(m2.matches()){
					WikipediaRow row = new WikipediaRow();
					row.setHref(m2.group(1));
					row.setTitle(m2.group(2));
					row.setName(m2.group(3));
					row.setAlternativeName(m2.group(4));
					row.setLifeSpan(m2.group(5));
					return row;
				}
				
				LOGGER.warn("Ill-formed line: '" + line + "'.");
				return NULL_ENTRY_1;
			}
			
		}){

			@Override
			protected boolean filterOut(WikipediaRow element) {
				return element == NULL_ENTRY_1;
			}
			
		}){

			@Override
			protected WikipediaComposer map(WikipediaRow from) {
				String lifeSpan = from.getLifeSpan();
				
				Matcher m1 = P1_2.matcher(lifeSpan);
				if(m1.matches()){
					WikipediaComposer toReturn = new WikipediaComposer();
					toReturn.setHref(from.getHref());
					toReturn.setTitle(from.getTitle());
					toReturn.setName(from.getName());
					toReturn.setAlternativeName(from.getAlternativeName());
					
					String group1 = m1.group(1);
					String group2 = m1.group(2);
					String group3 = m1.group(3);
					String group4 = m1.group(4);
					String group5 = m1.group(5);
					String group6 = m1.group(6);
					String group7 = m1.group(7);
					String group8 = m1.group(8);
					String group9 = m1.group(9);
					
					short firstDate = getDate(group3, group4);
					byte firstFlags = getFlags(group2, group5);
					short secondDate = getDate(group7, group8);
					byte secondFlags = getFlags(group6, group9);
					
					if(group1 == null){
						toReturn.setBirthyear(firstDate);
						toReturn.setBirthyearFlags(firstFlags);
						toReturn.setDeathyear(secondDate);
						toReturn.setDeathyearFlags(secondFlags);
					}
					else {
						toReturn.setPeriodType(PeriodType.FLORUIT);
						toReturn.setPeriodBegin(firstDate);
						toReturn.setPeriodBeginFlags(firstFlags);
						toReturn.setPeriodEnd(secondDate);
						toReturn.setPeriodEndFlags(secondFlags);
					}
					
					return toReturn;
				}
				
				Matcher m2 = P2_2.matcher(lifeSpan);
				if(m2.matches()){
					WikipediaComposer toReturn = new WikipediaComposer();
					toReturn.setHref(from.getHref());
					toReturn.setTitle(from.getTitle());
					toReturn.setName(from.getName());
					toReturn.setAlternativeName(from.getAlternativeName());
					
					String group1 = m2.group(1);
					String group2 = m2.group(2);
					String group3 = m2.group(3);
					String group4 = m2.group(4);
					String group5 = m2.group(5);
					
					short date = getDate(group3, group4);
					byte flags = getFlags(group2, group5);
					
					if("born".equals(group1)){
						toReturn.setBirthyear(date);
						toReturn.setBirthyearFlags(flags);
					}
					else {
						toReturn.setDeathyear(date);
						toReturn.setDeathyearFlags(flags);
					}
					
					return toReturn;
				}
				
				LOGGER.warn("Ill-formed life span: '" + lifeSpan + "'.");
				return NULL_ENTRY_2;
			}
			
			private short getDate(String year, String BC){
				return (short)(Short.parseShort(year) * (BC == null ? 1 : -1));
			}
			
			private byte getFlags(String information, String unsure){
				return TimeFlags.createFlags(information == null ? null : TimeInformation.parse(information), unsure != null);
			}
			
		}); 
	}
	
	@Override
	protected boolean filterOut(WikipediaComposer element) {
		return element == NULL_ENTRY_2;
	}

}
