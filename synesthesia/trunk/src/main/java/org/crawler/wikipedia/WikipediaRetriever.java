package org.crawler.wikipedia;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import org.crawler.IRetriever;
import org.util.LineIterator;

public class WikipediaRetriever implements IRetriever<WikipediaComposer> {
	
	private final static String URL = "https://en.wikipedia.org/wiki/List_of_composers_by_name";

	@Override
	public Iterator<WikipediaComposer> retrieve() throws IOException {
		return new WikipediaIterator(new LineIterator<String>(new URL(URL).openStream()){

			@Override
			protected String parseLine(String line) {
				return line;
			}
			
		});
	}

}
