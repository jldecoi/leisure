package org.crawler.wikipedia;

import org.crawler.dm.PeriodType;

public class WikipediaComposer {

	private String href;
	private String title;
	private String name;
	private String alternativeName;
	private Short birthyear;
	private Byte birthyearFlags;
	private PeriodType periodType;
	private Short periodBegin;
	private Byte periodBeginFlags;
	private Short periodEnd;
	private Byte periodEndFlags;
	private Short deathyear;
	private Byte deathyearFlags;
	
	public String getHref() {
		return href;
	}
	
	public void setHref(String href) {
		this.href = href;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAlternativeName() {
		return alternativeName;
	}
	
	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}
	
	public Short getBirthyear() {
		return birthyear;
	}
	
	public void setBirthyear(Short birthyear) {
		this.birthyear = birthyear;
	}
	
	public Byte getBirthyearFlags() {
		return birthyearFlags;
	}
	
	public void setBirthyearFlags(Byte birthyearFlags) {
		this.birthyearFlags = birthyearFlags;
	}
	
	public PeriodType getPeriodType() {
		return periodType;
	}
	
	public void setPeriodType(PeriodType periodType) {
		this.periodType = periodType;
	}
	
	public Short getPeriodBegin() {
		return periodBegin;
	}
	
	public void setPeriodBegin(Short periodBegin) {
		this.periodBegin = periodBegin;
	}
	
	public Byte getPeriodBeginFlags() {
		return periodBeginFlags;
	}
	
	public void setPeriodBeginFlags(Byte periodBeginFlags) {
		this.periodBeginFlags = periodBeginFlags;
	}
	
	public Short getPeriodEnd() {
		return periodEnd;
	}
	
	public void setPeriodEnd(Short periodEnd) {
		this.periodEnd = periodEnd;
	}
	
	public Byte getPeriodEndFlags() {
		return periodEndFlags;
	}
	
	public void setPeriodEndFlags(Byte periodEndFlags) {
		this.periodEndFlags = periodEndFlags;
	}
	
	public Short getDeathyear() {
		return deathyear;
	}
	
	public void setDeathyear(Short deathyear) {
		this.deathyear = deathyear;
	}
	
	public Byte getDeathyearFlags() {
		return deathyearFlags;
	}
	
	public void setDeathyearFlags(Byte deathyearFlags) {
		this.deathyearFlags = deathyearFlags;
	}
	
}
