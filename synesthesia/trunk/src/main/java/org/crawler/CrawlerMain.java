package org.crawler;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.art.spotify.data.FullArtist;
import org.art.spotify.data.SimplifiedAlbum;
import org.art.spotify.data.SimplifiedArtist;
import org.art.spotify.data.SimplifiedTrack;
import org.crawler.wga.WGAAuthor;
import org.crawler.wga.WGARetriever;
import org.crawler.wga.WGAStorer;
import org.crawler.wga.WGAWork;
import org.crawler.wikipedia.WikipediaComposer;
import org.crawler.wikipedia.WikipediaRetriever;
import org.crawler.wikipedia.WikipediaStorer;
import org.util.DBWrapper;
import org.util.SpotifyUtils;

public class CrawlerMain {
	
	public void crawl() throws IOException, SQLException, InterruptedException{
		//crawlPictures();
		crawlMusic();
	}
	
	private void crawlPictures() throws IOException, SQLException{
		new Crawler<Entry<WGAAuthor, List<WGAWork>>>(new WGARetriever(), new WGAStorer()).crawl();
	}
	
	private void crawlMusic() throws IOException, SQLException, InterruptedException {
		crawlWikipedia();
		crawlSpotify();
	}
	
	private void crawlWikipedia() throws IOException, SQLException {
		new Crawler<WikipediaComposer>(new WikipediaRetriever(), new WikipediaStorer()).crawl();
	}
	
	private void crawlSpotify() throws SQLException, IOException, InterruptedException{
		Statement statement = DBWrapper.createStatement();
		statement.execute("DROP TABLE IF EXISTS spotifyTracks");
		statement.execute("DROP TABLE IF EXISTS spotifyAlbums");
		statement.execute("DROP TABLE IF EXISTS spotifyArtists");
		statement.execute("CREATE TABLE spotifyArtists(id VARCHAR(22), composer INTEGER, confirmed BOOLEAN, fullArtist BLOB )");
		statement.execute("CREATE TABLE spotifyAlbums(id VARCHAR(22), artist VARCHAR(22), simplifiedAlbum BLOB )");
		statement.execute("CREATE TABLE spotifyTracks(id VARCHAR(22), album VARCHAR(22), confirmed BOOLEAN, simplifiedTrack BLOB )");
		crawlSpotifyArtists();
		crawlSpotifyAlbums();
		crawlSpotifyTracks();
	}
	
	private void crawlSpotifyArtists() throws SQLException, IOException, InterruptedException{		
		PreparedStatement insertArtist = DBWrapper.prepareStatement("INSERT INTO spotifyArtists (id, composer, confirmed, fullArtist) VALUES (?, ?, ?, ?)");
		SpotifyUtils utils = new SpotifyUtils();
		ResultSet namesByComposer = DBWrapper.createStatement().executeQuery("SELECT composer, ARRAY_AGG(name) FROM composerNames GROUP BY composer");
		while(namesByComposer.next()){
			int composer = namesByComposer.getInt(1);
			Object[] names = (Object[])namesByComposer.getArray(2).getArray();
			
			Map<String, FullArtist> artistsById = new HashMap<String, FullArtist>();
			for(Object name : names)
				for(FullArtist artist : utils.getArtists((String)name))
					artistsById.put(artist.getId(), artist);
					
			boolean confirmed = artistsById.size() == 1;
			for(Entry<String, FullArtist> entry : artistsById.entrySet()){
			   Blob blob = toBlob(entry.getValue());
			   
				insertArtist.setString(1, entry.getKey());
				insertArtist.setInt(2, composer);
				insertArtist.setBoolean(3, confirmed);
				insertArtist.setBlob(4, blob);
				insertArtist.execute();
                
                blob.free();
			}
			
		}		
	}
	
	private <X> Blob toBlob(X object) throws IOException, SQLException{
       Blob toReturn = DBWrapper.createBlob();
       ObjectOutputStream stream = new ObjectOutputStream(toReturn.setBinaryStream(1));
       stream.writeObject(object);
       stream.close();
       return toReturn;
	}

	private void crawlSpotifyAlbums() throws SQLException, IOException, InterruptedException{		
		PreparedStatement insertAlbums = DBWrapper.prepareStatement("INSERT INTO spotifyAlbums (id, artist, simplifiedAlbum) VALUES (?, ?, ?)");
		SpotifyUtils utils = new SpotifyUtils();
		ResultSet artists = DBWrapper.createStatement().executeQuery("SELECT DISTINCT id FROM spotifyArtists WHERE confirmed = TRUE");
		while(artists.next()){
			String artist = artists.getString(1);
			
			for(SimplifiedAlbum album : utils.getAlbums(artist)){
               Blob blob = toBlob(album);
               
				insertAlbums.setString(1, album.getId());
				insertAlbums.setString(2, artist);
				insertAlbums.setBlob(3, blob);
				insertAlbums.execute();
				
				blob.free();
			}
		}		
	}

	private void crawlSpotifyTracks() throws SQLException, IOException, InterruptedException{		
		PreparedStatement insertTracks = DBWrapper.prepareStatement("INSERT INTO spotifyTracks (id, album, confirmed, simplifiedTrack) VALUES (?, ?, ?, ?)");
		SpotifyUtils utils = new SpotifyUtils();
		ResultSet artistsAndAlbums = DBWrapper.createStatement().executeQuery("SELECT ARRAY_AGG(t1.id), ARRAY_AGG(t2.id) FROM spotifyArtists t1, spotifyAlbums t2 WHERE t1.id = t2.artist AND t1.confirmed = TRUE GROUP BY t1.composer");
		while(artistsAndAlbums.next()){
			Object[] artists = (Object[])artistsAndAlbums.getArray(1).getArray();
			Object[] albums = (Object[])artistsAndAlbums.getArray(2).getArray();
			
			for(Object albumObject : albums){
				String album = (String)albumObject;
				for(SimplifiedTrack track : utils.getTracks(album)){
	               Blob blob = toBlob(track);
	               
					insertTracks.setString(1, track.getId());
					insertTracks.setString(2, album);
					insertTracks.setBoolean(3, isByEitherOf(track, artists));
					insertTracks.setBlob(4, blob);
					insertTracks.execute();
	                
	                blob.free();
				}
			}
		}		
	}
	
	private boolean isByEitherOf(SimplifiedTrack track, Object[] artists){
		for(SimplifiedArtist artistObject : track.getArtists())
			for(Object artist : artists)
				if(artistObject.getId().equals(artist))
					return true;
		
		return false;
	}
	
	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException, InterruptedException {
		DBWrapper.init();
		new CrawlerMain().crawl();
		DBWrapper.close();
	}

}
