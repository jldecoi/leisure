package org.crawler;

import java.io.IOException;
import java.util.Iterator;

public interface IRetriever<X> {
	
	public Iterator<X> retrieve() throws IOException;

}
