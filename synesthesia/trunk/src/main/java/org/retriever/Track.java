package org.retriever;

public class Track extends AbstractWork {
	
	private long durationMs;

	public long getDurationMs() {
		return durationMs;
	}

	public void setDurationMs(long durationMs) {
		this.durationMs = durationMs;
	}

}
