package org.retriever;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.art.spotify.data.SimplifiedTrack;
import org.util.DBWrapper;
import org.util.MergingIterator;
import org.util.ResultSetIterator;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RetrieverMain implements Closeable {
	
	private final static String SPOTIFY_URL = "https://play.spotify.com/track/";

	private String authorFile;
	private String composerFile;
	
	private ObjectMapper mapper;

	public RetrieverMain(String authorFile, String composerFile) throws ClassNotFoundException, SQLException {
		DBWrapper.init();
		this.authorFile = authorFile;
		this.composerFile = composerFile;
		mapper = new ObjectMapper();
	}

	@Override
	public void close() throws IOException {
		try {
			DBWrapper.close();
		} catch (SQLException e) {
			throw new IOException(e);
		}
	}
	
	public void retrieve() throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		int[] timespan = getTimespan();
		int begin = timespan[0];
		int end = timespan[1];
		retrieveAuthors(begin, end);
		retrieveComposers(begin, end);
	}
	
	private int[] getTimespan() throws SQLException{
		Statement statement = DBWrapper.createStatement();
		ResultSet results = statement
				.executeQuery("SELECT MIN(birthYear), MIN(periodBegin), MAX(periodEnd), MAX(deathyear) FROM authors");
		results.next();
		int min1 = Math.min(results.getInt(1), results.getInt(2));
		int max1 = Math.max(results.getInt(3), results.getInt(4));

		results = statement
				.executeQuery("SELECT MIN(birthYear), MIN(periodBegin), MAX(periodEnd), MAX(deathyear) FROM composers");
		results.next();
		int min2 = Math.min(results.getInt(1), results.getInt(2));
		int max2 = Math.max(results.getInt(3), results.getInt(4));

		return new int[]{ Math.max(min1, min2), Math.min(max1, max2) };
	}

	private void retrieveAuthors(int begin, int end) throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		PreparedStatement prepared = DBWrapper.prepareStatement(
				"SELECT t1.id, t1.name, t1.birthYear, t1.periodBegin, t1.periodEnd, t1.deathyear, t2.title, t2.url FROM authors t1, works t2 WHERE t1.id = t2.author AND ( t1.birthYear >= ? OR t1.periodBegin >= ? ) AND ( t1.periodEnd <= ? OR t1.deathYear <= ? ) ORDER BY t1.id");
		prepared.setInt(1, begin);
		prepared.setInt(2, begin);
		prepared.setInt(3, end);
		prepared.setInt(4, end);

		Iterator<Author> iterator = new MergingIterator<AuthorRow, Author>(
				new ResultSetIterator<AuthorRow>(prepared.executeQuery()) {

					@Override
					protected AuthorRow getNext(ResultSet results) throws SQLException {
						Short birthyear = results.getShort(3);
						if(results.wasNull())
							birthyear = null;
						Short periodBegin = results.getShort(4);
						if(results.wasNull())
							periodBegin = null;
						Short periodEnd = results.getShort(5);
						if(results.wasNull())
							periodEnd = null;
						Short deathyear = results.getShort(6);
						if(results.wasNull())
							deathyear = null;
						
						AuthorRow toReturn = new AuthorRow();
						toReturn.setId(results.getInt(1));
						toReturn.setName(results.getString(2));
						toReturn.setBirthyear(birthyear);
						toReturn.setPeriodBegin(periodBegin);
						toReturn.setPeriodEnd(periodEnd);
						toReturn.setDeathyear(deathyear);
						toReturn.setTitle(results.getString(7));
						toReturn.setUrl(results.getString(8));
						return toReturn;
					}

				}) {

			@Override
			protected boolean belongTogether(AuthorRow first, AuthorRow second) {
				return first.getId() == second.getId();
			}

			@Override
			protected Author merge(List<AuthorRow> list) {
				// "list" is never empty;
				AuthorRow first = list.get(0);
				Short birthyear = first.getBirthyear();
				Short deathyear = first.getDeathyear();
				short begin = birthyear == null ? first.getPeriodBegin() : birthyear;
				short end = deathyear == null ? first.getPeriodEnd() : deathyear;

				List<Work> works = new ArrayList<Work>();
				for (AuthorRow row : list) {
					Work work = new Work();
					work.setTitle(row.getTitle());
					work.setURL(row.getUrl());

					works.add(work);
				}

				Author author = new Author();
				author.setName(first.getName());
				author.setBegin(begin);
				author.setEnd(end);
				author.setWorks(works);
				return author;
			}

		};
		List<Author> authors = new ArrayList<Author>();
		while (iterator.hasNext())
			authors.add(iterator.next());
		mapper.writeValue(new File(authorFile), authors);
	}
    
    private <X> X fromBlob(Blob blob, Class<X> _class) throws IOException, SQLException, ClassNotFoundException{
       ObjectInputStream stream = new ObjectInputStream(blob.getBinaryStream());
       X toReturn = _class.cast(stream.readObject());
       stream.close();
       return toReturn;
    }
	
	private void retrieveComposers(int begin, int end) throws SQLException, JsonGenerationException, JsonMappingException, IOException{
		PreparedStatement prepared = DBWrapper.prepareStatement(
				"SELECT t1.composer, t1.names, t2.birthyear, t2.periodBegin, t2.periodEnd, t2.deathyear, t2.trackId, t2.track FROM ( SELECT composer composer, ARRAY_AGG(name) names FROM composerNames GROUP BY composer ) t1, ( SELECT t0.id composerId, t0.birthyear birthyear, t0.periodBegin, t0.periodEnd, t0.deathyear deathyear, t3.id trackId, t3.simplifiedTrack track FROM composers t0, spotifyartists t1, spotifyalbums t2, spotifytracks t3 WHERE t0.id = t1.composer AND t1.id = t2.artist AND t2.id = t3.album AND t1.confirmed = TRUE AND t3.confirmed = TRUE AND ( t0.birthYear >= ? OR t0.periodBegin >= ? ) AND ( t0.periodEnd <= ? OR t0.deathYear <= ? ) ) t2 WHERE t1.composer = t2.composerId");
		prepared.setInt(1, begin);
		prepared.setInt(2, begin);
		prepared.setInt(3, end);
		prepared.setInt(4, end);

		Iterator<Composer> iterator = new MergingIterator<ComposerRow, Composer>(
				new ResultSetIterator<ComposerRow>(prepared.executeQuery()) {

					@Override
					protected ComposerRow getNext(ResultSet results) throws SQLException {
						Short birthyear = results.getShort(3);
						if(results.wasNull())
							birthyear = null;
						Short periodBegin = results.getShort(4);
						if(results.wasNull())
							periodBegin = null;
						Short periodEnd = results.getShort(5);
						if(results.wasNull())
							periodEnd = null;
						Short deathyear = results.getShort(6);
						if(results.wasNull())
							deathyear = null;
						
						SimplifiedTrack track;
                  try {
                     track = fromBlob(results.getBlob(8), SimplifiedTrack.class);
                  }
                  catch ( ClassNotFoundException | IOException e ) {
                     throw new RuntimeException(e);
                  }
						
						ComposerRow toReturn = new ComposerRow();
						toReturn.setId(results.getInt(1));
						toReturn.setName((String)((Object[])results.getArray(2).getArray())[0]);
						toReturn.setBirthyear(birthyear);
						toReturn.setPeriodBegin(periodBegin);
						toReturn.setPeriodEnd(periodEnd);
						toReturn.setDeathyear(deathyear);
						toReturn.setUrl(results.getString(7));
						toReturn.setTitle(track.getName());
						toReturn.setDurationMs(track.getDuration_ms().longValue());
						return toReturn;
					}

				}) {

			@Override
			protected boolean belongTogether(ComposerRow first, ComposerRow second) {
				return first.getId() == second.getId();
			}

			@Override
			protected Composer merge(List<ComposerRow> list) {
				// "list" is never empty;
				ComposerRow first = list.get(0);
				Short birthyear = first.getBirthyear();
				Short deathyear = first.getDeathyear();
				short begin = birthyear == null ? first.getPeriodBegin() : birthyear;
				short end = deathyear == null ? first.getPeriodEnd() : deathyear;

				List<Track> works = new ArrayList<Track>();
				for (ComposerRow row : list) {
					Track track = new Track();
					track.setTitle(row.getTitle());
					track.setURL(SPOTIFY_URL + row.getUrl());
					track.setDurationMs(row.getDurationMs());

					works.add(track);
				}

				Composer composer = new Composer();
				composer.setName(first.getName());
				composer.setBegin(begin);
				composer.setEnd(end);
				composer.setWorks(works);
				return composer;
			}

		};
		List<Composer> composers = new ArrayList<Composer>();
		while (iterator.hasNext())
			composers.add(iterator.next());
		mapper.writeValue(new File(composerFile), composers);
	}

	public static void main(String[] args)
			throws SQLException, ClassNotFoundException, JsonGenerationException, JsonMappingException, IOException {
		String path = "src/main/resources/public/";
		RetrieverMain retriever = new RetrieverMain(path + "authors.js", path + "composers.js");
		retriever.retrieve();
		retriever.close();
	}

}
