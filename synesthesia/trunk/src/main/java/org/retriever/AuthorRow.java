package org.retriever;

public class AuthorRow {

	private int id;
	private String name;
	private Short birthyear;
	private Short periodBegin;
	private Short periodEnd;
	private Short deathyear;
	private String title;
	private String url;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Short getBirthyear() {
		return birthyear;
	}
	
	public void setBirthyear(Short birthyear) {
		this.birthyear = birthyear;
	}
	
	public Short getPeriodBegin() {
		return periodBegin;
	}
	
	public void setPeriodBegin(Short periodBegin) {
		this.periodBegin = periodBegin;
	}
	
	public Short getPeriodEnd() {
		return periodEnd;
	}
	
	public void setPeriodEnd(Short periodEnd) {
		this.periodEnd = periodEnd;
	}
	
	public Short getDeathyear() {
		return deathyear;
	}
	
	public void setDeathyear(Short deathyear) {
		this.deathyear = deathyear;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

}
