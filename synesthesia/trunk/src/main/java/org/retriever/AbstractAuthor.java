package org.retriever;

import java.util.List;

public abstract class AbstractAuthor<X extends AbstractWork> {
	
	private String name;
	private int begin;
	private int end;
	private List<X> works;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getBegin() {
		return begin;
	}
	
	public void setBegin(int begin) {
		this.begin = begin;
	}
	
	public int getEnd() {
		return end;
	}
	
	public void setEnd(int end) {
		this.end = end;
	}
	
	public List<X> getWorks() {
		return works;
	}
	
	public void setWorks(List<X> works) {
		this.works = works;
	}
	
}
