CREATE TABLE spotifyTracks(
	id VARCHAR(22),
	album VARCHAR(22),
	confirmed BOOLEAN,
	simplifiedTrack BLOB
)
