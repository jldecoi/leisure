CREATE TABLE composerNames(
	id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	composer INTEGER,
	name CHARACTER VARYING(256),
	FOREIGN KEY (composer) REFERENCES composers ( id ) 
)
