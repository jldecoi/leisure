CREATE TABLE spotifyArtists(
	id VARCHAR(22),
	composer INTEGER,
	confirmed BOOLEAN,
	fullArtist BLOB
)
