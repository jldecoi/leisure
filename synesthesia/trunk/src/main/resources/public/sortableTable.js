var sorters = [
	{
		property : "name",
		sortingFunction : function(first, second){
			return first < second ? -1 : first == second ? 0 : 1
		},
		ascending : true
	},
	{
		property : "begin",
		sortingFunction : function(first, second){
			return first < second ? -1 : first == second ? 0 : 1
		},
		ascending : true
	},
	{
		property : "end",
		sortingFunction : function(first, second){
			return first < second ? -1 : first == second ? 0 : 1
		},
		ascending : true
	}
]

function valueAll(array, isTo){
	var toReturn = isTo ? Number.NEGATIVE_INFINITY : Number.POSITIVE_INFINITY
	for(var i=0; i<array.length; i++){
		var element = array[i];
		var begin = element.begin
		var end = element.end
		if(isTo && end > toReturn)
			toReturn = end
		else if(!isTo && begin < toReturn)
			toReturn = begin
	}
	return toReturn
}

function init(isTo){
	var valueComposers = valueAll(composers, isTo);
	var valueAuthors = valueAll(authors, isTo);
	return isTo ? Math.max(valueComposers, valueAuthors) : Math.min(valueComposers, valueAuthors)
}

var DEFAULT_APPROXIMATE_LENGTH = 30
var DEFAULT_APPROXIMATE_TIME_PER_IMAGE = 30
var DEFAULT_FROM = init(false)
var DEFAULT_TO = init(true)

var approximateLength = DEFAULT_APPROXIMATE_LENGTH
var approximateTimePerImage = DEFAULT_APPROXIMATE_TIME_PER_IMAGE
var from = DEFAULT_FROM
var to = DEFAULT_TO

function checkInteger(value){
	return !isNaN(parseInt(value))
}

var alternatives = [
	{
		name : "composer",
		listener : function(){
			var main = document.getElementById("dynamic")
			main.replaceChild(createTable(composers, sorters), main.childNodes[0])
		}
	},
	{
		name : "artist",
		listener : function(){
			var main = document.getElementById("dynamic")
			main.replaceChild(createTable(authors, sorters), main.childNodes[0])
		}
	},
	{
		name : "time period",
		listener : function(){
			var main = document.getElementById("dynamic")
			main.replaceChild(createTimeSpan(), main.childNodes[0])
		}
	}
]

function createInit(){
	var div = document.createElement("div")
	
	var p0 = document.createElement("p")
	p0.appendChild(document.createTextNode("Approximate length (m): "))
	
	var input0 = document.createElement("input")
	input0.value = approximateLength
	input0.onchange = function(){
		var value = this.value
		if(!checkInteger(value) || value <= 0){
			alert("The value must be a strictly positive integer.")
			this.value = DEFAULT_APPROXIMATE_LENGTH
		}
		else approximateLength = value
	}
	p0.appendChild(input0)
	
	var p1 = document.createElement("p")
	p1.appendChild(document.createTextNode("Approximate time per image (s): "))
	
	var input1 = document.createElement("input")
	input1.value = approximateTimePerImage
	input1.onchange = function(){
		var value = this.value
		if(!checkInteger(value) || value <= 0){
			alert("The value must be a strictly positive integer.")
			this.value = DEFAULT_APPROXIMATE_TIME_PER_IMAGE
		}
		else approximateTimePerImage = value
	}
	p1.appendChild(input1)
	
	var p2 = document.createElement("p")
	p2.appendChild(document.createTextNode("Search by"))
	
	var ul = document.createElement("ul")
	for (var i=0; i<alternatives.length; i++) {
		var alternative = alternatives[i]
		
		var li = document.createElement("li")
		var input = document.createElement("input")
		input.type = "radio"
		input.name = "search"
		input.onclick = alternative.listener
		
		li.appendChild(input)
		li.appendChild(document.createTextNode(" " + alternative.name))
		
		ul.appendChild(li)
	}

	div.appendChild(p0)
	div.appendChild(p1)
	div.appendChild(p2)
	div.appendChild(ul)
	return div
}

function sort(model, sorter){
	model.sort(function(first, second){
		var property = sorter.property
		var toReturn = sorter.sortingFunction(first[property], second[property])
		return sorter.ascending ? toReturn : -toReturn
	})
	sorter.ascending = !sorter.ascending
}

function replaceMain(model, sorter){
	return function(){
		sort(model, sorter)
		var main = document.getElementById("dynamic")
		main.replaceChild(createTable(model, sorters), main.childNodes[0])
	}
}

function setTimeouts(window, works, timeout){
	var length = works.length
	if(length == 0)
		return //else
		
	var head = works[0]
	window.location.href = head.url
	var tail = works.slice(1)
	setTimeout(function(){
		setTimeouts(window, tail, timeout)
	}, timeout == null ? head.durationMs : timeout)
}

function nextElement(array){
	return array[Math.floor(array.length * Math.random())]
}

function nextTrack(tracks, maxLength){
	var toReturn
	do{
		toReturn = nextElement(tracks)
	} while(toReturn.durationMs > maxLength)
	return toReturn
}

function selectTracks(allTracks){
	var toReturn = []
	
	var play = approximateLength * 60 * 100;
	var remainingTime = 10 * play
	while(remainingTime > play){
		track = nextTrack(allTracks, remainingTime + play)
		remainingTime -= track.durationMs
		toReturn.push(track)
	}
	
	return toReturn
}

function selectImages(allImages, number){
	var toReturn = []
	for(var i=0; i<number; i++)
		toReturn.push(nextElement(allImages))
	return toReturn
}

function start(allTracks, allImages){
	var tracks = selectTracks(allTracks)
	var overallLength = 0
	for(var i=0; i<tracks.length; i++)
		overallLength += tracks[i].durationMs
		
	var images = selectImages(allImages, Math.round(overallLength / approximateTimePerImage / 1000))
	var averageLength = overallLength / images.length
	
	setTimeouts(window.open(), tracks, null)
	setTimeouts(window.open(), images, averageLength)
}

function addListener(model, index){
	var tracks
	var images
	
	if(model == composers){
		var composer = model[index]
		var begin = composer.begin
		var end = composer.end
		tracks = composer.works
		
		images = []
		for(var i=0; i<authors.length; i++){
			var author = authors[i]
			if(!(author.end < begin || author.begin > end))
				images = images.concat(author.works)
		}

		return function(){
			start(tracks, images)
		}
	} //else
	
	if(model == authors){
		var author = model[index]
		var begin = author.begin
		var end = author.end
		images = author.works
		
		tracks = []
		for(var i=0; i<composers.length; i++){
			var composer = composers[i]
			if(!(composer.end < begin || composer.begin > end))
				tracks = tracks.concat(composer.works)
		}

		return function(){
			start(tracks, images)
		}
	} //else
	
	return function(){
		tracks = []
		for(var i=0; i<composers.length; i++){
			var composer = composers[i]
			if(!(composer.end < from || composer.begin > to))
				tracks = tracks.concat(composer.works)
		}
		
		images = []
		for(var i=0; i<authors.length; i++){
			var author = authors[i]
			if(!(author.end < from || author.begin > to))
				images = images.concat(author.works)
		}
		start(tracks, images)
	}
}

function createTable(model, sorters){
	var table = document.createElement("table")
	table.border = 1
	table.style["border-spacing"] = 0
	
	var thead = document.createElement("thead")
	
	var tr = document.createElement("tr")
	for (var i=0; i<sorters.length; i++) {
		var th = document.createElement("th")
		th.onclick = replaceMain(model, sorters[i])
		th.appendChild(document.createTextNode(sorters[i].property))
		
		tr.appendChild(th)
	}
	thead.appendChild(tr)
	
	var tbody = document.createElement("tbody")
	for (var i=0; i<model.length; i++) {
		var modelEntry = model[i]
		var tr = document.createElement("tr")
		
		for (var j=0; j<sorters.length; j++) {
			var td = document.createElement("td")
			
			var value = modelEntry[sorters[j].property]
			var child
			if(j == 0){
				child = document.createElement("input")
				child.type = "button"
				child.value = value
				child.onclick = addListener(model, i)
			}
			else child = document.createTextNode(value)
			
			td.appendChild(child)
			tr.appendChild(td)
		}
		
		tbody.appendChild(tr)
	}
	
	table.appendChild(thead)
	table.appendChild(tbody)
	return table
}

function createTimeSpan(){
	var p = document.createElement("p")
	
	var input1 = document.createElement("input")
	input1.value = from
	input1.onchange = function(){
		var value = this.value
		if(!checkInteger(value)){
			alert("The value must be an integer.")
			this.value = DEFAULT_FROM
		}
		else if(value < DEFAULT_FROM){
			alert("The value must be greater or equal to " + DEFAULT_FROM + ".")
			this.value = DEFAULT_FROM
		}
		else from = value
	}
	
	var input2 = document.createElement("input")
	input2.value = to
	input2.onchange = function(){
		var value = this.value
		if(!checkInteger(value)){
			alert("The value must be an integer.")
			this.value = DEFAULT_TO
		}
		else if(value > DEFAULT_TO){
			alert("The value must be less or equal to " + DEFAULT_TO + ".")
			this.value = DEFAULT_TO
		}
		else to = value
	}

	var input = document.createElement("input")
	input.type = "button"
	input.value = "Submit"
	input.onclick = addListener(null)
	
	p.appendChild(document.createTextNode("From: "))
	p.appendChild(input1)
	p.appendChild(document.createTextNode(" to: "))
	p.appendChild(input2)
	p.appendChild(input)
	return p
}

function main(){
	document.getElementById("static").appendChild(createInit())
}
