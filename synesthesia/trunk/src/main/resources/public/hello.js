function Forms($scope, $http) {
	$http.get("forms").success(function(data) {
		$scope.forms = data;
	});
}

function Schools($scope, $http) {
	$http.get("schools").success(function(data) {
		$scope.schools = data;
	});
}

function TimeFrames($scope, $http) {
	$http.get("timeFrames").success(function(data) {
		$scope.timeFrames = data;
	});
}

function Types($scope, $http) {
	$http.get("types").success(function(data) {
		$scope.types = data;
	});
}

function WorksByAuthor($scope, $http) {
	$http.get("worksByAuthor" + window.location.search).success(function(data) {
		
		$scope.worksByAuthor = data;
		$scope.index = 0;
		
		$scope.getAuthors = function(){
			var toReturn = [];
			
			for (var i = 0; i < $scope.worksByAuthor.length; i++){
				var keys = Object.keys($scope.worksByAuthor[i]);
				for(var j=0; j<keys.length; j++)
					toReturn.push(keys[j]);
			}
				
			return toReturn;
		}
		
		$scope.getWorks = function(){
			for (var i = 0; i < $scope.worksByAuthor.length; i++){
				var object = $scope.worksByAuthor[i];
				if(object.hasOwnProperty($scope.selectedAuthor)){
					var works = object[$scope.selectedAuthor];
					var length = works.length;

					$scope.work = works[$scope.index % length];
					return;
				}
			}
		}
		
		
		
	});
}

var URIs;
var index;

function Uris($scope, $http) {
	$http.get("uris" + window.location.search).success(
			function(data) {
				URIs = data;
				index = 0;
				main();
			});
}

function main() {
	document.getElementById("img").src = URIs[index % URIs.length];
	index++;

	setTimeout(main, 20000);
}
